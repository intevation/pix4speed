// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Package version holds the version information.
package version

// SemVersion the version in semver.org format, SHOULD be overwritten during
// the linking stage of the build process.
var SemVersion = "2.0.4"

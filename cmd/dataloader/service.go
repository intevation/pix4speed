// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"fmt"
	"net/http"
	"net/http/cgi"
	"os"
	"strings"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

const (
	// deliveryNoteKey triggers the process for updatinf or
	// inserting a dop.
	deliveryNoteKey = "deliverynotelist"

	// deliveryNoteListKey signals that a list of available
	// delivery notes should be delivered.
	deliveryNoteListKey = "deliveryNoteListKey"

	// clearSystemTimeKey signals tat the emergency stop file
	// should be cleared.
	clearSystemTimeKey = "clearSystemTimeKey"

	// setSystemTimeKey signals that the emergency stop file
	// should be set.
	setSystemTimeKey = "setSystemTimeKey"
)

// isCGI determines if the program is started as a cgi program.
func isCGI() bool {
	v, ok := os.LookupEnv("GATEWAY_INTERFACE")
	if !ok {
		return false
	}
	return strings.HasPrefix(strings.TrimSpace(v), "CGI")
}

// runAsCGI binds the handler to serve the incomming http request.
func runAsCGI() {
	check(cgi.Serve(http.HandlerFunc(handler)))
}

func serveText(rw http.ResponseWriter, format string, args ...any) {
	rw.WriteHeader(http.StatusOK)
	rw.Header().Set("Content-Type", "text/plain")
	fmt.Fprintf(rw, format, args...)
}

func asQuotedString(list []string) string {
	var b strings.Builder
	b.WriteByte('[')
	for i, item := range list {
		if i > 0 {
			b.WriteByte(',')
		}
		b.WriteByte('\'')
		b.WriteString(item)
		b.WriteByte('\'')
	}
	b.WriteByte(']')
	return b.String()
}

// handler makes it possible to
// * read all the delivery notes,
// * set and reset the system time equals emergency stop or
// process a delivery note.
func handler(rw http.ResponseWriter, req *http.Request) {

	cfg, err := loadConfig(defaultProperties)
	if err != nil {
		log.Errorf("%v\n", err)
		http.Error(rw, "Config loading failed. See log files for details.",
			http.StatusInternalServerError)
		return
	}

	dl := newDataLoader(cfg)

	// The given delivery note will be processed.
	if dnk := req.FormValue(deliveryNoteKey); dnk != "" {
		if err := dl.loadDataFromDeliveryNote(dnk); err != nil {
			log.Errorf("%v\n", err)
		}
		serveText(rw, "%t", err == nil)
		return
	}

	// The list of delivery notes is requested and will be returned.
	if req.FormValue(deliveryNoteListKey) != "" {
		list, err := dl.getAllAvailableDeliveryNotes()
		if err != nil {
			log.Errorf("%v\n", err)
			http.Error(
				rw, "getAllAvailableDeliveryNotes failed. See log files for details.",
				http.StatusInternalServerError)
			return
		}
		serveText(rw, "%s", asQuotedString(list))
		return
	}

	// The emergency stop file will be removed.
	if req.FormValue(clearSystemTimeKey) != "" {
		err := dl.clearEmergencyStop()
		if err != nil {
			log.Errorf("%v\n", err)
		}
		serveText(rw, "%t", err == nil)
		return
	}

	// The ermergency stop file will be created.
	if req.FormValue(setSystemTimeKey) != "" {
		startTime, err1 := parseTime(req.FormValue("start"))
		if err != nil {
			log.Errorf("Starttime is invalid: %v\n", err1)
		}
		endTime, err2 := parseTime(req.FormValue("end"))
		if err2 != nil {
			log.Errorf("Enddtime is invalid: %v\n", err2)
		}
		var err3 error
		if err1 == nil && err2 == nil {
			if err3 = dl.writeEmergencyStopFile(startTime, endTime); err != nil {
				log.Errorf("%v\n", err3)
			}
		}
		serveText(rw, "%t", err1 == nil && err2 == nil && err3 == nil)
		return
	}
}

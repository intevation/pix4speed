// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"flag"
	"fmt"
	"os"
	"path/filepath"

	"heptapod.host/intevation/pix4speed/pkg/log"
	"heptapod.host/intevation/pix4speed/pkg/version"
)

func usage() {
	basename := filepath.Base(os.Args[0])
	fmt.Fprintf(os.Stderr, `Usage:
%[1]s cron                                - for starting the cron to update dops
%[1]s loadmetadatafile <MetadataFileName> - to insert or update a single metadata-file into database
%[1]s initpyramid                         - to initialize a new Image-Pyramid

Option:
-p propertiesFile | --properties=propertiesFile - to use a different Properties file
-l level | --loglevel=level                     - set log level
-f file  | --logfile=file                       - set log file ("-" for STDOUT,
                                                                prefix with "+" to add a second output).
-V | --version                                  - show version information
`, basename)
}

func runAsCommandLine() {

	var (
		propertiesFile string
		logFile        string
		logLevel       string
		showVersion    bool
	)

	flag.StringVar(
		&propertiesFile, "p", defaultProperties, "properties file")
	flag.StringVar(
		&propertiesFile, "properties", defaultProperties, "properties file")
	flag.StringVar(
		&logLevel, "l", "", "log level (shorthand)")
	flag.StringVar(
		&logFile, "f", "", "log file (shorthand)")
	flag.StringVar(
		&logLevel, "loglevel", "", "log level")
	flag.StringVar(
		&logFile, "logfile", "", "log file")
	flag.BoolVar(
		&showVersion, "version", false, "show version")
	flag.BoolVar(
		&showVersion, "V", false, "show version (shorthand)")

	flag.Usage = usage

	flag.Parse()

	if showVersion {
		fmt.Printf("version: %s\n", version.SemVersion)
		os.Exit(0)
	}

	var (
		mode         string
		metadataFile string
		rest         []string
	)

	for _, arg := range flag.Args() {
		switch arg {
		case "cron", "loadmetadatafile", "initpyramid":
			mode = arg
		default:
			rest = append(rest, arg)
		}
	}

	if len(rest) == 1 && mode == "loadmetadatafile" {
		metadataFile = rest[0]
		rest = rest[:1]
	}

	if len(rest) > 0 {
		fmt.Printf("error: unused command line argument(s) %q after mode\n",
			rest)
		os.Exit(2)
	}

	cfg, err := loadConfig(propertiesFile)
	check(err)

	if logLevel != "" {
		level, err := log.ParseLogLevel(logLevel)
		check(err)
		log.SetLogLevel(level)
	}
	if logFile != "" {
		check(log.SetupLog(logFile, 0644))
	}

	dl := newDataLoader(cfg)

	switch mode {
	case "cron":
		check(dl.bulkLoad())
	case "loadmetadatafile":
		if metadataFile == "" {
			log.Fatalln("error: missing metadata file")
		}
		if cfg.DataLoader.UseSimpleMetaData {
			mdp := newMetaDataParser(cfg)
			check(mdp.insertShortMetaData(metadataFile))
			log.Infoln("Save short Meta Data succeed")
		}
	case "initpyramid":
		check(dl.initPyramid())
	}
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"errors"
	"fmt"
	"io/fs"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"sync"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

// rasterGeneralizer provides the generalization functions for an
// image pyramid.
// You can create a new pyramid calling calculateAll
// or update a tiff in the pyramid calling updateAll.
type rasterGeneralizer struct {
	cfg             *config
	recalcIndexLock sync.Mutex
	fileLocks       locks[levelFile]
}

func newRasterGeneralizer(cfg *config) *rasterGeneralizer {
	return &rasterGeneralizer{
		cfg: cfg,
		fileLocks: locks[levelFile]{
			Less: levelFile.Less,
		},
	}
}

// xy stores a 2D point.
type xy struct {
	x int
	y int
}

// inside returns true if c is inside a box spanned by a and b.
func (c xy) inside(a, b xy) bool {
	return a.x <= c.x && c.x <= b.x &&
		a.y <= c.y && c.y <= b.y
}

// less returns true if c is less than d componentwise.
func (c xy) less(d xy) bool {
	if c.x != d.x {
		return c.x < d.x
	}
	return c.y < d.y
}

func (c xy) String() string {
	return fmt.Sprintf("%d, %d", c.x, c.y)
}

// xyFile pairs a coordinate with a filename.
type xyFile struct {
	coord xy
	file  string
}

// xyFiles is a list of coordinate attributed filenames.
type xyFiles []xyFile

// sort orders the xy files by there coordinates componentwise.
func (xyfs xyFiles) sort() {
	sort.Slice(xyfs, func(i, j int) bool {
		return xyfs[i].coord.less(xyfs[j].coord)
	})
}

type levelFile struct {
	level int
	file  string
}

func (lf levelFile) Less(o levelFile) bool {
	if lf.level != o.level {
		return lf.level < o.level
	}
	return lf.file < o.file
}

// ensureDirExists ensures that a given directory exists.
func ensureDirExists(dirname string) error {
	if _, err := os.Stat(dirname); err != nil {
		// Does it exist?
		if errors.Is(err, fs.ErrNotExist) {
			// Try to create it.
			if err := os.MkdirAll(dirname, 0755); err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

// calculateAll initializes a new image pyramid.
func (rg *rasterGeneralizer) calculateAll() error {

	log.Debugln("calculateAll has been called")

	// First prepare the origin data. Create an internal tiling and
	// ompress all tiffs and write them to folder for level zero.
	if err := rg.prepareAllOriginData(); err != nil {
		log.Errorln(
			"Preparing all data failed. Process will be stopped")
		return err
	}

	// Then create overviews to all tifs in level zero.
	if err := rg.createOverviewsLevelZero(); err != nil {
		log.Errorln(
			"Creating overviews for all data failed. Process will  stopped")
		return err
	}

	// Then calculate all levels of the pyramid.
	var (
		startLevel = rg.cfg.DataLoader.StartLevel
		endLevel   = rg.cfg.DataLoader.EndLevel
		extent     = rg.cfg.DataLoader.Extent * rg.cfg.DataLoader.scale()
	)
	for levelStep := startLevel; levelStep <= endLevel; levelStep++ {

		sourceDir := filepath.Join(
			rg.cfg.Default.PyramidFolder, strconv.Itoa(levelStep))

		targetDirStep := levelStep + 1
		targetDir := filepath.Join(
			rg.cfg.Default.PyramidFolder, strconv.Itoa(targetDirStep))

		if _, err := os.Stat(sourceDir); err != nil {
			log.Errorf("Cannot stat %q\n", err)
			return fmt.Errorf("calculateAll: %w", err)
		}

		if err := ensureDirExists(targetDir); err != nil {
			log.Errorf("Cannot create target folder %q\n", targetDir)
			return fmt.Errorf("calculateAll: %w", err)
		}

		// For level zero we have to use the origin naming of the files.
		var sourceFileNameTemplate string
		if levelStep == 0 {
			sourceFileNameTemplate = rg.cfg.DataLoader.FileNameTemplate
		} else {
			sourceFileNameTemplate = rg.cfg.DataLoader.AlternativeFileNameTemplate
		}
		targetFileNameTemplate := rg.cfg.DataLoader.AlternativeFileNameTemplate

		log.Debugf("source dir: %q\n", sourceDir)
		log.Debugf("target dir: %q\n", targetDir)
		log.Debugf("fileNameTemplate: %s\n", rg.cfg.DataLoader.FileNameTemplate)
		log.Debugf("extent: %d\n", extent)

		if err := rg.generalizeRaster(
			extent,
			sourceFileNameTemplate,
			targetFileNameTemplate,
			sourceDir, targetDir,
		); err != nil {
			log.Errorf(
				"Creating level %d failed. Process will be stopped\n",
				levelStep)
			return err
		}

		extent *= rg.cfg.DataLoader.Step
	}

	// Re-calculate the tile indexes.
	return rg.recalculateIndex()
}

// recalculate executes the command fore re-calculating
// the tile indexes (shapefiles) for all levels.
func (rg *rasterGeneralizer) recalculateIndex() error {
	log.Debugln("recalculateIndex has been called")
	if !rg.cfg.RasterGeneralizer.RecalculateIndexEnabled {
		return nil
	}
	rg.recalcIndexLock.Lock()
	defer rg.recalcIndexLock.Unlock()

	return executeCommand(
		rg.cfg.RasterGeneralizer.RecalculateIndexExecutable,
		rg.cfg.Default.PyramidFolder,
		rg.cfg.RasterGeneralizer.RecalculateIndexIndexDir)
}

// generalizeRaster creates a complete new level of the pyramid.
func (rg *rasterGeneralizer) generalizeRaster(
	extent int,
	sourceTemplate, targetTemplate,
	sourceDir, targetDir string,
) error {
	log.Debugln("generalizeRaster has been called")

	xyToFile, err := rg.createXYToFile(sourceDir)
	if err != nil {
		return fmt.Errorf("generalizeRaster: %w", err)
	}

	log.Debugf("Number of files to process: %d\n", len(xyToFile))

	if len(xyToFile) == 0 {
		log.Debugf("No input files found in source directory\n")
		return nil
	}

	var (
		scale = rg.cfg.DataLoader.scale()
		minX  = rg.cfg.DataLoader.MinX * scale
		maxX  = rg.cfg.DataLoader.MaxX * scale
		minY  = rg.cfg.DataLoader.MinY * scale
		maxY  = rg.cfg.DataLoader.MaxY * scale
		delta = rg.cfg.DataLoader.Step * extent
	)

	if delta <= 0 {
		return fmt.Errorf("delta too small: step: %d extent: %d",
			rg.cfg.DataLoader.Step, extent)
	}

	var (
		errs   []error
		workCh = make(chan xy)
		errCh  = make(chan error)
		done   = make(chan struct{})
		wg     sync.WaitGroup
	)

	go func() {
		defer close(done)
		for err := range errCh {
			errs = append(errs, err)
		}
	}()

	for i, n := 0, rg.cfg.DataLoader.threadsCount(); i < n; i++ {
		wg.Add(1)
		go rg.generalizeWorker(
			&wg, workCh, errCh,
			sourceDir, targetDir,
			sourceTemplate, targetTemplate,
			extent,
			xyToFile,
		)
	}

	for tempX := minX; tempX <= maxX; tempX += delta {
		for tempY := minY; tempY <= maxY; tempY += delta {
			workCh <- xy{x: tempX, y: tempY}
		}
	}
	close(workCh)

	wg.Wait()
	close(errCh)
	<-done

	if len(errs) == 0 {
		log.Debugln("generalizeRaster has been finished")
		return nil
	}

	for _, err := range errs {
		log.Errorf("generalizeRaster failed: %v\n", err)
	}

	return fmt.Errorf(
		"generalizeRaster: failed with %d errors", len(errs))
}

func (rg *rasterGeneralizer) generalizeWorker(
	wg *sync.WaitGroup,
	workList <-chan xy,
	errs chan<- error,
	sourceDir, targetDir string,
	sourceTemplate, targetTemplate string,
	extent int,
	xyToFile xyFiles,
) {
	defer wg.Done()
	for coord := range workList {
		log.Debugf("Working on %s\n", coord)
		if err := rg.generalizePyramidBrick(
			coord,
			sourceDir, targetDir,
			sourceTemplate, targetTemplate,
			extent,
			xyToFile,
		); err != nil {
			errs <- fmt.Errorf(
				"working on %s failed: %w", coord, err)
		}
		log.Debugf("Working on %s done\n", coord)
	}
}

// generalizePyramidBrick creates one brick in a level of the pyramid.
func (rg *rasterGeneralizer) generalizePyramidBrick(
	coord xy,
	sourceDir, targetDir string,
	sourceTemplate, targetTemplate string,
	extent int,
	xyToFile xyFiles,
) error {
	files := rg.determineFiles(coord, extent, xyToFile)

	if len(files) == 0 {
		log.Debugln("No files exist for new raster\n")
		return nil
	}

	log.Debugf("%v\n", files)

	return rg.processFiles(
		files,
		sourceDir, targetDir,
		sourceTemplate, targetTemplate,
		coord)
}

// determineFiles determines all source files which should be merged
// to one new dop.
func (rg *rasterGeneralizer) determineFiles(
	coord xy,
	extent int,
	xyToFile xyFiles,
) []string {
	if len(xyToFile) == 0 {
		return nil
	}
	end := rg.cfg.DataLoader.Step * extent
	var files []string
	top := xy{
		x: coord.x + end - 1, // -1 to exclude upper border
		y: coord.y + end - 1,
	}
	for _, f := range xyToFile {
		if f.coord.inside(coord, top) {
			files = append(files, f.file)
		}
	}
	return files
}

// processFiles merges the given files to one new dop.
func (rg *rasterGeneralizer) processFiles(
	files []string,
	sourceDir, targetDir string,
	sourceTemplate, targetTemplate string,
	coord xy,
) error {
	log.Debugln("processFiles has been called")

	vrtFileName := rg.buildFilename(targetTemplate, coord, ".vrt")
	tiffFileName := rg.buildFilename(targetTemplate, coord, ".tif")

	// First create a virtual raster table.
	if err := rg.createVrt(
		files,
		sourceDir, targetDir,
		vrtFileName,
		coord,
	); err != nil {
		log.Errorln("Creating vrt failed. Processing files be will canceled")
		return fmt.Errorf("processFiles failed: %w", err)
	}

	// Then merge the files to one new dop using the virual raster.
	if err := rg.mergeFiles(vrtFileName, tiffFileName, targetDir); err != nil {
		log.Errorf(
			"Creating tiff %q failed. Processing files will be canceled", tiffFileName)
		return fmt.Errorf("processFiles failed. %w", err)
	}

	// create external overviews to the new dop.
	return rg.createOverviews(filepath.Join(targetDir, tiffFileName))
}

// mergeFiles creates a new dop using the data from a virtual raster table.
func (rg *rasterGeneralizer) mergeFiles(
	vrtFileName, dopFileName, targetDir string,
) error {
	log.Debugln("mergeFiles has been called")

	gdalTranslate := rg.cfg.RasterGeneralizer.gdalTranslate()

	args := append(append(
		[]string(nil),
		rg.cfg.RasterGeneralizer.GdalTranslateParams...),
		filepath.Join(targetDir, vrtFileName),
		filepath.Join(targetDir, dopFileName))

	return executeCommand(gdalTranslate, args...)
}

// createVrt creates a virtual raster table.
func (rg *rasterGeneralizer) createVrt(
	files []string,
	sourceDir, targetDir string,
	vrtFileName string,
	coord xy,
) error {
	log.Debugln("createVrt has been called")

	gdalbuildvrt := rg.cfg.RasterGeneralizer.gdalbuildvrt()

	args := append(append(
		[]string(nil), rg.cfg.RasterGeneralizer.GdalbuildvrtParams...),
		filepath.Join(targetDir, vrtFileName))

	for _, file := range files {
		args = append(args, filepath.Join(sourceDir, file))
	}

	return executeCommand(gdalbuildvrt, args...)
}

// buildFilenamebuilds a filename from template x/y and
// extension used for vrt and tiff filenames.
func (rg *rasterGeneralizer) buildFilename(
	fileNameTemplate string,
	coord xy,
	extension string,
) string {
	prefix, _, _ := strings.Cut(fileNameTemplate, "_")
	scale := rg.cfg.DataLoader.scale()
	x, y := coord.x/scale, coord.y/scale
	return fmt.Sprintf("%s_%d_%d%s", prefix, x, y, extension)
}

// createXYToFile looks into every tif file for x/y coords
// and saves the result in a x/y -> file name map.
func (rg *rasterGeneralizer) createXYToFile(dir string) (xyFiles, error) {
	log.Debugf("createXYToFile(%q) calling\n", dir)

	tiffs, err := rg.getAllTiffFiles(dir)
	if err != nil {
		return nil, fmt.Errorf("createXYToFile: %w", err)
	}

	result := make(xyFiles, len(tiffs))

	for i, tiff := range tiffs {
		file := filepath.Join(dir, tiff)
		coord, err := rg.extractXYFromFile(file)
		if err != nil {
			return nil, fmt.Errorf("createXYToFile: %w", err)
		}
		result[i] = xyFile{coord: coord, file: tiff}
	}
	result.sort()

	return result, nil
}

func (rg *rasterGeneralizer) extractXYFromFile(tiff string) (xy, error) {

	tmp := newTIFFMetaParser(rg.cfg)
	tm, err := tmp.parseFile(tiff)
	if err != nil {
		return xy{}, fmt.Errorf("extractXYFromFile: %w", err)
	}

	var (
		xform  = tm.GeoTransform
		width  = tm.Size[0]
		height = tm.Size[1]
		minX   = xform[0] + rg.cfg.DataLoader.ProjXOffset
		minY   = xform[3] + width*xform[4] + height*xform[5]
	)

	return xy{
		x: int(math.Round(minX)),
		y: int(math.Round(minY)),
	}, nil
}

// createOverviewsLevelZero calculates external overviews for all dops
// whice are located in level zero.
func (rg *rasterGeneralizer) createOverviewsLevelZero() error {
	log.Debugln("createOverviewsLevelZero has been called")
	folder := filepath.Join(rg.cfg.Default.PyramidFolder, "0")
	files, err := rg.getAllTiffFiles(folder)
	if err != nil {
		return fmt.Errorf("createOverviewsLevelZero failed: %w", err)
	}
	for _, file := range files {
		f := filepath.Join(folder, file)
		if err := rg.createOverviews(f); err != nil {
			log.Errorf("Creating overviews for %q failed.\n", f)
			return err
		}
	}
	return nil
}

// createOverviews creates the external overviews to a new dop.
func (rg *rasterGeneralizer) createOverviews(dop string) error {
	log.Debugln("createOverviews has been called")

	if !rg.cfg.RasterGeneralizer.UseGdaladdo {
		log.Infoln("Not creating overviews as it is deactivated.")
		return nil
	}

	gdaladdo := rg.cfg.RasterGeneralizer.gdaladdo()

	args := append(append(append([]string(nil),
		rg.cfg.RasterGeneralizer.GdaladdoParams...),
		dop),
		rg.cfg.RasterGeneralizer.gdaladdoLevels()...)

	return executeCommand(gdaladdo, args...)
}

// prepareAllOriginData prepares all the origin data during the
// process of intialisation of a new image pyramid.
func (rg *rasterGeneralizer) prepareAllOriginData() error {
	log.Debugln("prepareAllOriginData has been called")

	sourceDir := rg.cfg.Default.DataFolder
	targetDir := filepath.Join(rg.cfg.Default.PyramidFolder, "0")

	// Check if source dir can be accessed.
	if _, err := os.Stat(sourceDir); err != nil {
		log.Errorf("Cannot stat source dir %q\n", sourceDir)
		return fmt.Errorf("prepareAllOriginData: %q: %w", sourceDir, err)
	}
	// Check if target dir can be accessed.
	if err := ensureDirExists(targetDir); err != nil {
		log.Errorf("Cannot create target folder %q\n", targetDir)
		return fmt.Errorf("prepareAllOriginData: %q: %w", targetDir, err)
	}

	files, err := rg.getAllTiffFiles(sourceDir)
	if err != nil {
		return fmt.Errorf("prepareAllOriginData: %w", err)
	}

	var wg sync.WaitGroup

	var (
		errs    []error
		filesCh = make(chan string)
		errCh   = make(chan error)
		doneCh  = make(chan struct{})
	)

	go func() {
		defer close(doneCh)
		for err := range errCh {
			errs = append(errs, err)
		}
	}()

	for i, n := 0, rg.cfg.DataLoader.threadsCount(); i < n; i++ {
		wg.Add(1)
		go rg.prepareAllOriginDataWorker(
			&wg, filesCh, errCh,
			sourceDir, targetDir)
	}

	for _, file := range files {
		filesCh <- file
	}
	close(filesCh)

	wg.Wait()
	close(errCh)
	<-doneCh

	if len(errs) == 0 {
		return nil
	}

	for _, err := range errs {
		log.Errorf("Preparing failed: %v\n", err)
	}

	return fmt.Errorf("preparing of %d files failed", len(errs))
}

func (rg *rasterGeneralizer) prepareAllOriginDataWorker(
	wg *sync.WaitGroup,
	files <-chan string,
	errs chan<- error,
	sourceDir, targetDir string,
) {
	defer wg.Done()
	for file := range files {
		log.Debugf("Working on %q\n", file)
		if err := rg.prepareOriginData(sourceDir, targetDir, file); err != nil {
			errs <- fmt.Errorf("%q: %w", file, err)
		}
		log.Debugf("Working on %q done\n", file)
	}
}

// prepareOriginData prepares the origin data of a dop.
func (rg *rasterGeneralizer) prepareOriginData(
	sourceDir, targetDir, tiff string,
) error {
	log.Debugln("prepareOriginData has been called")

	gdalTranslate := rg.cfg.RasterGeneralizer.gdalTranslate()

	args := append(append(
		[]string(nil),
		rg.cfg.RasterGeneralizer.GdalTranslateOriginParams...),
		filepath.Join(sourceDir, tiff),
		filepath.Join(targetDir, tiff))

	if err := executeCommand(gdalTranslate, args...); err != nil {
		log.Debugln("prepareOriginData has failed")
		return err
	}

	if rg.cfg.RasterGeneralizer.DeleteOriginData {
		file := strings.TrimSuffix(tiff, ".tif")
		for _, f := range []string{tiff, file + ".tfw", file + ".xml"} {
			if err := os.Remove(filepath.Join(sourceDir, f)); err != nil {
				return fmt.Errorf("deleting %q failed: %w", f, err)
			}
		}
	}

	return nil
}

// executeCommand exectutes an external program.
func executeCommand(name string, args ...string) error {
	if log.DebugLogLevel >= log.GetLogLevel() {
		log.Debugf("%s %s\n", name, strings.Join(args, " "))
	}
	cmd := exec.Command(name, args...)
	if err := cmd.Run(); err != nil {
		var ee *exec.ExitError
		if errors.As(err, &ee) {
			return fmt.Errorf("%s terminated with %d: %s",
				name, ee.ProcessState.ExitCode(), ee.Stderr)
		}
		return err
	}
	return nil
}

// getAllTiffFiles extracts all tiff files found in a given folder.
func (rg *rasterGeneralizer) getAllTiffFiles(folder string) ([]string, error) {
	log.Debugln("getAllTiffFiles has been called")
	entries, err := os.ReadDir(folder)
	if err != nil {
		return nil, err
	}
	var tiffs []string
	for _, entry := range entries {
		if name := entry.Name(); strings.HasSuffix(name, ".tif") && entry.Type().IsRegular() {
			tiffs = append(tiffs, name)
		}
	}
	return tiffs, nil
}

// updateAll updates a single tif file in the image pyramid.
func (rg *rasterGeneralizer) updateAll(tiffFileName string) error {
	log.Debugln("updateAll has been called")

	// The min coordinate of the given tiff.
	var coord xy

	log.Debugf("Lock file %q\n", tiffFileName)
	if err := rg.fileLocks.lock(func() error {

		log.Debugln("prepare origin data")

		zeroFolder := filepath.Join(rg.cfg.Default.PyramidFolder, "0")
		// The given tiff file will be prepared to be used in the
		// image pyramid. It will be moded into folder of level zero.
		if err := ensureDirExists(zeroFolder); err != nil {
			log.Errorf("Cannot create folder %q\n", zeroFolder)
			return fmt.Errorf("updateAll: %w", err)
		}
		if err := rg.prepareOriginData(
			rg.cfg.Default.DataFolder,
			zeroFolder,
			tiffFileName,
		); err != nil {
			log.Errorln("Preparing originData failed. Update will be stopped")
			return fmt.Errorf("updateAll: %w", err)
		}

		zeroTiff := filepath.Join(zeroFolder, tiffFileName)
		var err error
		if coord, err = rg.extractXYFromFile(zeroTiff); err != nil {
			log.Errorf("Cannot determin coordinates of %q\n", zeroTiff)
			return fmt.Errorf("updateAll: %w", err)
		}

		// The overview will be calculated to the new file.
		log.Debugf("Calculate overview for source file %q\n", zeroTiff)
		if err := rg.createOverviews(zeroTiff); err != nil {
			log.Errorf("Calculating overview failed. Update will be stopped\n")
		}

		return nil
	}, levelFile{level: 0, file: tiffFileName}); err != nil {
		return err
	}

	// All levels will be partially updated.
	var (
		startLevel = rg.cfg.DataLoader.StartLevel
		endLevel   = rg.cfg.DataLoader.EndLevel
		extent     = rg.cfg.DataLoader.Extent
		step       = rg.cfg.DataLoader.Step
	)
	for levelStep := startLevel; levelStep <= endLevel; levelStep++ {
		log.Debugln("###########################################")
		log.Debugf("Calculate level %d\n", levelStep)
		log.Debugln("###########################################")

		sourceDir := filepath.Join(
			rg.cfg.Default.PyramidFolder, strconv.Itoa(levelStep))

		targetDirStep := levelStep + 1
		targetDir := filepath.Join(
			rg.cfg.Default.PyramidFolder, strconv.Itoa(targetDirStep))

		var sourceFileNameTemplate string
		if levelStep == 0 {
			sourceFileNameTemplate = rg.cfg.DataLoader.FileNameTemplate
		} else {
			sourceFileNameTemplate = rg.cfg.DataLoader.AlternativeFileNameTemplate
		}
		targetFileNameTemplate := rg.cfg.DataLoader.AlternativeFileNameTemplate

		// Determine minX und minY of the area which must be
		// updated in the current level.
		var (
			raster = xy{
				x: (coord.x - rg.cfg.DataLoader.MinX) / (step * extent),
				y: (coord.y - rg.cfg.DataLoader.MinY) / (step * extent),
			}
			pos = xy{
				x: rg.cfg.DataLoader.MinX + raster.x*step*extent,
				y: rg.cfg.DataLoader.MinY + raster.y*step*extent,
			}
		)

		log.Debugf("Step: %d\n", step)
		log.Debugf("Extent: %d\n", extent)
		log.Debugf("FileX: %d\n", coord.x)
		log.Debugf("FileY: %d\n", coord.y)
		log.Debugf("Raster: %s\n", raster)
		log.Debugf("Pos: %s\n", pos)

		if err := rg.updateRasterProcess(
			pos,
			sourceFileNameTemplate, targetFileNameTemplate,
			sourceDir, targetDir,
			extent, levelStep,
		); err != nil {
			log.Errorf("Updating raster for level %d failed. "+
				"Process will be canceled\n", levelStep)
			return fmt.Errorf("updateAll: %w", err)
		}

		extent *= step
	}

	// Re-calculate the tile indexes.
	return rg.recalculateIndex()
}

// updateRasterProcess does a partial update of a pyramid level.
// Only the files which are touched by the given coordinate values
// will be re-calculated.
func (rg *rasterGeneralizer) updateRasterProcess(
	coord xy,
	sourceTemplate, targetTemplate string,
	sourceDir, targetDir string,
	extent, levelStep int,
) error {
	xyToFile, err := rg.createXYToFile(sourceDir)
	if err != nil {
		return fmt.Errorf("updateRasterProcess: %w", err)
	}
	files := rg.determineFiles(coord, extent, xyToFile)
	if len(files) == 0 {
		log.Debugln("No files exist to update raster")
		return nil
	}
	log.Debugf("%v\n", files)
	tiffFile := rg.buildFilename(targetTemplate, coord, ".tif")
	// Files to lock.
	lfs := append(
		levelFiles(files, levelStep),
		levelFile{level: levelStep, file: tiffFile})
	if err := rg.fileLocks.lock(func() error {
		return rg.processFiles(
			files,
			sourceDir, targetDir,
			sourceTemplate, targetTemplate,
			coord)
	}, lfs...); err != nil {
		log.Errorf(
			"Processing files %v failed. Update will be canceled\n",
			files)
		return fmt.Errorf("updateRasterProcess: %w", err)
	}
	return nil
}

// levelFiles decorates files with a level
func levelFiles(files []string, level int) []levelFile {
	lfs := make([]levelFile, len(files))
	for i, file := range files {
		lfs[i] = levelFile{level: level, file: file}
	}
	return lfs
}

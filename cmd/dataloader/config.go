// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"regexp"
	"runtime"
	"strconv"
	"sync"

	"github.com/BurntSushi/toml"
	"github.com/google/shlex"
	pgx "github.com/jackc/pgx/v5"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

// ManagerPropertiesEnv is an env var to specify the loaction
// of the configuration properties file.
const ManagerPropertiesEnv = "ManagerProperties"

type defaultCfg struct {
	HomeFolder    string `toml:"homeFolder"`
	DataFolder    string `toml:"dataFolder"`
	PyramidFolder string `toml:"pyramidFolder"`
	ShapeFolder   string `toml:"shapeFolder"`
}

type dataLoaderCfg struct {
	BulkDeliveryNoteFolder      string  `toml:"bulkDeliveryNoteFolder"`
	DeliveryNoteFolder          string  `toml:"deliveryNoteFolder"`
	DeliveryNoteFolderProcessed string  `toml:"deliveryNoteFolderProcessed"`
	MinX                        int     `toml:"minX"`
	MinY                        int     `toml:"minY"`
	MaxX                        int     `toml:"maxX"`
	MaxY                        int     `toml:"maxY"`
	Step                        int     `toml:"step"`
	Extent                      int     `toml:"extent"`
	Scale                       int     `toml:"scale"`
	FileNameTemplate            string  `toml:"fileNameTemplate"`
	AlternativeFileNameTemplate string  `toml:"alternativeFileNameTemplate"`
	StartLevel                  int     `toml:"startlevel"`
	EndLevel                    int     `toml:"endlevel"`
	PIDFile                     string  `toml:"pidFile"`
	EmergencyStopFile           string  `toml:"emergencyStopFile"`
	ProcessFile                 bool    `toml:"processFile"`
	ValidateTIFFFiles           bool    `toml:"validate_tiff_files"`
	ValidateXMLFiles            bool    `toml:"validate_xml_files"`
	ValidateTFWFiles            bool    `toml:"validate_tfw_files"`
	ProjXOffset                 float64 `toml:"proj_x_offset"`
	UseSimpleMetaData           bool    `toml:"useSimpleMetaData"`
	XMLTIFExtraChars            string  `toml:"xml_tif_extra_chars"`
	ThreadsCount                int     `toml:"threads_count"`
}

type dataValidatorCfg struct {
	PixelSize  float64 `toml:"pixelSize"`
	RasterSize int     `toml:"rasterSize"`
}

type parsedParams []string

type rasterGeneralizerCfg struct {
	GdalHome                   string       `toml:"gdal_home"`
	GdalInfoExecutable         string       `toml:"gdalinfo"`
	GdalbuildvrtExecutable     string       `toml:"gdal_gdalbuildvrt"`
	GdalbuildvrtParams         parsedParams `toml:"gdal_gdalbuildvrtParams"`
	GdalTranslateExecutable    string       `toml:"gdal_gdal_translateExecutable"`
	GdalTranslateParams        parsedParams `toml:"gdal_gdal_translateParams"`
	UseGdaladdo                bool         `toml:"use_gdaladdo"`
	GdaladdoExecutable         string       `toml:"gdal_gdaladdoExecutable"`
	GdaladdoParams             parsedParams `toml:"gdal_gdaladdoParams"`
	GdaladdoLevels             []int        `toml:"gdal_gdaladdoLevels"`
	GdalTranslateOriginParams  parsedParams `toml:"gdal_gdal_translateOriginParams"`
	RecalculateIndexEnabled    bool         `toml:"recalculate_Index_Enabled"`
	RecalculateIndexExecutable string       `toml:"recalculate_Index_Executable"`
	RecalculateIndexIndexDir   string       `toml:"recalculate_Index_IndexDir"`
	DeleteOriginData           bool         `toml:"deleteOriginData"`
}

type metaParserCfg struct {
	XSDFileName     string `toml:"xsdFileName"`
	XMLLint         string `toml:"xmllint"`
	DBUser          string `toml:"db_user"`
	DBCreds         string `toml:"db_creds"`
	DBName          string `toml:"db_name"`
	DBHost          string `toml:"db_host"`
	DBPort          int    `toml:"db_port"`
	DBMetaDataTable string `toml:"db_metadata_table"`
	DBEncoding      string `toml:"db_encoding"`
}

type loggingCfg struct {
	Level log.Level `toml:"level"`
	File  string    `toml:"file"`
}

type config struct {
	Default           defaultCfg           `toml:"DEFAULT"`
	DataLoader        dataLoaderCfg        `toml:"dataloader"`
	MetaParser        metaParserCfg        `toml:"metaparser"`
	DataValidator     dataValidatorCfg     `toml:"datavalidator"`
	RasterGeneralizer rasterGeneralizerCfg `toml:"rastergeneralizer"`
	Logging           loggingCfg           `toml:"logging"`
}

func (dlc *dataLoaderCfg) scale() int {
	if dlc.Scale > 0 {
		return dlc.Scale
	}
	return 1000
}

func (dlc *dataLoaderCfg) threadsCount() int {
	if dlc.ThreadsCount > 0 {
		return dlc.ThreadsCount
	}
	return runtime.NumCPU()
}

func (mp *metaParserCfg) dbMetaDataTable() string {
	if mdt := mp.DBMetaDataTable; mdt != "" {
		return mdt
	}
	return "metadata"
}

// hasDBConnection checks if a database connection is configured.
func (mp *metaParserCfg) hasDBConnection() bool {
	return mp.DBHost != "" ||
		mp.DBPort != 0 ||
		mp.DBName != "" ||
		mp.DBUser != "" ||
		mp.DBCreds != ""
}

// dbConnection establishes a database connection based
// on the given credentials.
func (mp *metaParserCfg) dbConnection(ctx context.Context) (*pgx.Conn, error) {
	cc, err := pgx.ParseConfig("")
	if err != nil {
		return nil, err
	}
	cc.Host = mp.DBHost
	cc.Port = uint16(mp.DBPort)
	cc.Database = mp.DBName
	cc.User = mp.DBUser
	cc.Password = mp.DBCreds
	return pgx.ConnectConfig(ctx, cc)
}

// UnmarshalText implements [encoding.TextUnmarshaler].
func (pp *parsedParams) UnmarshalText(text []byte) error {
	params, err := shlex.Split(string(text))
	if err != nil {
		return err
	}
	*pp = parsedParams(params)
	return nil
}

func (rgc *rasterGeneralizerCfg) prefixGdalHome(cmd string) string {
	if rgc.GdalHome != "" {
		return filepath.Join(rgc.GdalHome, cmd)
	}
	return cmd
}

func coalesce(a, b string) string {
	if a != "" {
		return a
	}
	return b
}

func (rgc *rasterGeneralizerCfg) gdalinfo() string {
	return rgc.prefixGdalHome(coalesce(rgc.GdalInfoExecutable, "gdalinfo"))
}

func (rgc *rasterGeneralizerCfg) gdalTranslate() string {
	return rgc.prefixGdalHome(coalesce(rgc.GdalTranslateExecutable, "gdal_translate"))
}

func (rgc *rasterGeneralizerCfg) gdaladdo() string {
	return rgc.prefixGdalHome(coalesce(rgc.GdaladdoExecutable, "gdaladdo"))
}

func (rgc *rasterGeneralizerCfg) gdalbuildvrt() string {
	return rgc.prefixGdalHome(coalesce(rgc.GdalbuildvrtExecutable, "gdalbuildvrt"))
}

func (rgc *rasterGeneralizerCfg) gdaladdoLevels() []string {
	levels := make([]string, len(rgc.GdaladdoLevels))
	for i, level := range rgc.GdaladdoLevels {
		levels[i] = strconv.Itoa(level)
	}
	return levels
}

var (
	resolvePat     *regexp.Regexp
	resolvePatOnce sync.Once
)

func compileResolvePat() {
	resolvePat = regexp.MustCompile(`%\([^)]+\)s`)
}

func resolve(elems []reflect.Value) error {
	replacements := map[string]string{}

	// first phase: index by toml tags.
	for _, elem := range elems {
		fields := reflect.VisibleFields(elem.Type())
		for i := range fields {
			field := &fields[i]
			f := elem.FieldByName(field.Name)
			if f.IsValid() && f.Kind() == reflect.String {
				if t := field.Tag.Get("toml"); t != "" {
					replacements[t] = f.String()
				}
			}
		}
	}

	resolvePatOnce.Do(compileResolvePat)

	replacer := func(s string) (r string, err error) {

		type tooDeep struct{}
		visited := map[string]struct{}{}

		var replace func(string) string
		replace = func(s string) string {
			stripped := s[2 : len(s)-2]
			if _, ok := visited[stripped]; ok {
				panic(tooDeep{})
			}
			if r, ok := replacements[stripped]; ok {
				visited[stripped] = struct{}{}
				r := resolvePat.ReplaceAllStringFunc(r, replace)
				delete(visited, stripped)
				return r
			}
			return s
		}

		defer func() {
			if x := recover(); x != nil {
				if _, ok := x.(tooDeep); ok {
					err = fmt.Errorf("resolving %q leeds to deep recursion", s)
				} else {
					panic(x)
				}
			}
		}()
		return resolvePat.ReplaceAllStringFunc(s, replace), nil
	}

	// second phase: apply replacements.
	for _, elem := range elems {
		fields := reflect.VisibleFields(elem.Type())
		for i := range fields {
			field := &fields[i]
			if field.Tag.Get("toml") == "" {
				continue
			}
			f := elem.FieldByName(field.Name)
			if !f.IsValid() {
				continue
			}
			switch f.Kind() {
			case reflect.String:
				r, err := replacer(f.String())
				if err != nil {
					return err
				}
				f.SetString(r)
			case reflect.Slice:
				for j, n := 0, f.Len(); j < n; j++ {
					sj := f.Index(j)
					if sj.Kind() != reflect.String {
						continue
					}
					r, err := replacer(sj.String())
					if err != nil {
						return err
					}
					sj.Addr().Elem().SetString(r)
				}
			}
		}
	}
	return nil
}

func (cfg *config) resolve() error {
	return resolve([]reflect.Value{
		reflect.ValueOf(&cfg.Default).Elem(),
		reflect.ValueOf(&cfg.DataLoader).Elem(),
		reflect.ValueOf(&cfg.MetaParser).Elem(),
		reflect.ValueOf(&cfg.DataValidator).Elem(),
		reflect.ValueOf(&cfg.RasterGeneralizer).Elem(),
		reflect.ValueOf(&cfg.Logging).Elem(),
	})
}

func loadConfig(propertiesFile string) (*config, error) {

	var cfg config

	if env, ok := os.LookupEnv(ManagerPropertiesEnv); ok {
		propertiesFile = env
	}

	if _, err := toml.DecodeFile(propertiesFile, &cfg); err != nil {
		return nil, err
	}

	if err := cfg.resolve(); err != nil {
		return nil, err
	}

	log.SetLogLevel(cfg.Logging.Level)
	if file := cfg.Logging.File; file != "" {
		if err := log.SetupLog(file, 0644); err != nil {
			return nil, fmt.Errorf("loadConfig: %w", err)
		}
	}

	return &cfg, nil
}

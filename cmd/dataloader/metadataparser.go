// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"context"
	"encoding/xml"
	"errors"
	"fmt"
	"io"
	"os"
	"strings"
	"time"

	pgx "github.com/jackc/pgx/v5"
	"golang.org/x/net/html/charset"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

// metaDataParser parses the meta data from an xml file
// and writes it into a postgis database.
// It supports insert and update of meta data.
type metaDataParser struct {
	cfg *config
}

func newMetaDataParser(cfg *config) *metaDataParser {
	return &metaDataParser{
		cfg: cfg,
	}
}

const (
	geometryColumnSQL = `
SELECT f_geometry_column FROM geometry_columns
WHERE f_table_name = $1`

	needsUpdateSQL = `
SELECT EXISTS (SELECT 1 from %s where kachelname = $1)`

	updateShortSQL = `
UPDATE %s SET %s = ST_GeomFromText($1), processing_starttime = $2
WHERE kachelname = $3`

	insertShortSQL = `
INSERT INTO %s (%s, processing_starttime, kachelname)
VALUES (ST_GeomFromText($1), $2, $3)`
)

func geometryColumn(ctx context.Context, tx pgx.Tx, metadata string) (string, error) {
	rows, err := tx.Query(ctx, geometryColumnSQL, metadata)
	if err != nil {
		return "", err
	}
	column, err := pgx.CollectOneRow(rows, func(row pgx.CollectableRow) (string, error) {
		var column string
		err := row.Scan(&column)
		return column, err
	})
	if errors.Is(err, pgx.ErrNoRows) {
		return "", nil
	}
	return column, err
}

func entryExists(ctx context.Context, tx pgx.Tx, metadata, kachel string) (bool, error) {
	rows, err := tx.Query(ctx, fmt.Sprintf(needsUpdateSQL, metadata), kachel)
	if err != nil {
		return false, err
	}
	exists, err := pgx.CollectOneRow(rows, func(row pgx.CollectableRow) (bool, error) {
		var exists bool
		err := row.Scan(&exists)
		return exists, err
	})
	if errors.Is(err, pgx.ErrNoRows) {
		return false, nil
	}
	return exists, nil
}

// insertShortMetaData does a short meta data insertion.
func (mdp *metaDataParser) insertShortMetaData(filename string) error {

	if !mdp.cfg.MetaParser.hasDBConnection() {
		log.Debugln("No database connection configured.")
		return nil
	}

	geometry, err := mdp.readGeometryFromTiff(filename)
	if err != nil {
		return fmt.Errorf("insertShortMetaData: %w", err)
	}

	ctx := context.Background()

	conn, err := mdp.cfg.MetaParser.dbConnection(ctx)
	if err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}
	defer conn.Close(ctx)

	tx, err := conn.Begin(ctx)
	if err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}
	defer tx.Rollback(ctx)

	metadata := mdp.cfg.MetaParser.dbMetaDataTable()

	geoCol, err := geometryColumn(ctx, tx, metadata)
	if err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}

	if geoCol == "" {
		return fmt.Errorf("no layer found for %q", metadata)
	}

	exists, err := entryExists(ctx, tx, metadata, filename)
	if err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}

	var sql string

	if exists {
		sql = fmt.Sprintf(updateShortSQL, metadata, geoCol)
	} else {
		sql = fmt.Sprintf(insertShortSQL, metadata, geoCol)
	}
	if _, err := tx.Exec(ctx, sql, geometry, time.Now().UTC(), filename); err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}

	return tx.Commit(ctx)
}

// readGeometryFromTiff reads the geometry reference of the metadata
// from the corresponding tiff file and returns it a WKT polygon.
func (mdp *metaDataParser) readGeometryFromTiff(tiffFileName string) (string, error) {
	tmp := newTIFFMetaParser(mdp.cfg)
	dataset, err := tmp.parseFile(tiffFileName)
	if err != nil {
		log.Errorf("No Dataset could be read from %q\n", tiffFileName)
		return "", err
	}
	var (
		geotransform = dataset.GeoTransform
		rxs          = dataset.Size[0]
		rys          = dataset.Size[1]
		startX       = geotransform[0]
		startY       = geotransform[3]
		rasterSizeX  = geotransform[1]
		rasterSizeY  = geotransform[5]
		width        = rxs * rasterSizeX
		height       = rys * rasterSizeY
	)

	wkt := fmt.Sprintf("POLYGON((%f %f, %f %f, %f %f, %f %f, %f %f))",
		startX, startY,
		startX, startY+height,
		startX+width, startY+height,
		startX+width, startY,
		startX, startY)

	return wkt, nil
}

// parseMetaData parses the meta data from a given xml file
// and inserts or updates it into the database.
func (mdp *metaDataParser) parseMetaData(xmlFile string) error {
	log.Debugln("parseMetaData has been called")

	fields, err := readFieldsFromXSD(mdp.cfg.MetaParser.XSDFileName)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}

	values, err := readValuesFromXML(xmlFile, fields)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}

	// Check if the xml file is valid to the given xml schema.
	if err := mdp.validateXMLFile(xmlFile); err != nil {
		log.Errorf("No valid XML for file %q.\n", xmlFile)
		return err
	}

	log.Infoln("Valid schema")

	if !mdp.cfg.MetaParser.hasDBConnection() {
		log.Debugln("No database connection configured.")
		return nil
	}

	tiffFileName := strings.TrimSuffix(xmlFile, ".xml") +
		mdp.cfg.DataLoader.XMLTIFExtraChars + ".tif"

	geometry, err := mdp.readGeometryFromTiff(tiffFileName)
	if err != nil {
		log.Errorf("Cannot load geometry from %q\n",
			tiffFileName)
		return err
	}

	kachelname := tiffFileName

	// Write the data into db.
	ctx := context.Background()

	conn, err := mdp.cfg.MetaParser.dbConnection(ctx)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}
	defer conn.Close(ctx)

	tx, err := conn.Begin(ctx)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}
	defer tx.Rollback(ctx)

	metadata := mdp.cfg.MetaParser.dbMetaDataTable()

	geoCol, err := geometryColumn(ctx, tx, metadata)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}

	if geoCol == "" {
		return fmt.Errorf("no layer found for %q", metadata)
	}

	exists, err := entryExists(ctx, tx, metadata, kachelname)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}

	cols, err := tableColumns(ctx, tx, metadata)
	if err != nil {
		return fmt.Errorf("parseMetaData failed: %w", err)
	}
	if len(cols) == 0 {
		return fmt.Errorf("no meta informations found for %q", metadata)
	}

	var sql string
	var args []any

	casts := map[string]cast{geoCol: stGeomFromText}

	setValue(values, geoCol, geometry)
	setValue(values, "status", "IN PROCESS")
	setValue(values, "processing_starttime", time.Now().UTC().Format(time.RFC3339))

	if exists {
		log.Debugln("Update is required")
		// In case someone wrote it into the xml file.
		deleteValue(values, "kachelname")
		sql, args = cols.updateStatement(
			metadata, values, casts,
			"kachelname", kachelname)
	} else {
		log.Debugln("Insert is required")
		setValue(values, "kachelname", kachelname)
		sql, args = cols.insertStatement(metadata, values, casts)
	}

	log.Debugf("SQL: %s\n", sql)

	if _, err := tx.Exec(ctx, sql, args...); err != nil {
		log.Errorln("Storing feature failed")
		return fmt.Errorf("parseMetaData failed: %w", err)
	}

	return tx.Commit(ctx)
}

func stGeomFromText(param string) string {
	return "ST_GeomFromText(" + param + ")"
}

// setProcessingStateByDOPName sets the processing state of
// a meta data entry using the given dopName as an identifier.
// It is possible to update the field status.
// The filed processing_endtime will be set to the current time.
func (mdp *metaDataParser) setProcessingStateByDOPName(
	success bool,
	dopName string,
) error {
	log.Debugln("setProcessingStateByDOPName has been called")

	if !mdp.cfg.MetaParser.hasDBConnection() {
		log.Debugln("No database connection configured.")
		return nil
	}

	ctx := context.Background()

	conn, err := mdp.cfg.MetaParser.dbConnection(ctx)
	if err != nil {
		return fmt.Errorf("setProcessingStateByDOPName failed: %w", err)
	}
	defer conn.Close(ctx)
	tx, err := conn.Begin(ctx)
	if err != nil {
		return fmt.Errorf("setProcessingStateByDOPName failed: %w", err)
	}
	defer func() {
		log.Debugln("setProcessingStateByDOPName has been finished")
		tx.Rollback(ctx)
	}()

	metadata := mdp.cfg.MetaParser.dbMetaDataTable()

	exists, err := entryExists(ctx, tx, metadata, dopName)
	if err != nil {
		return fmt.Errorf("insertShortMetaData failed: %w", err)
	}

	cols, err := tableColumns(ctx, tx, metadata)
	if err != nil {
		return fmt.Errorf("setProcessingStateByDOPName failed: %w", err)
	}
	if len(cols) == 0 {
		return fmt.Errorf("no meta informations found for %q", metadata)
	}

	var status string
	if success {
		status = "FINISHED"
	} else {
		status = "FAILED"
	}

	values := map[string]string{
		"status":             status,
		"processing_endtime": time.Now().UTC().Format(time.RFC3339),
	}

	var sql string
	var args []any

	if exists {
		sql, args = cols.updateStatement(metadata, values, nil, "kachelname", dopName)
	} else {
		values["kachelname"] = dopName
		sql, args = cols.insertStatement(metadata, values, nil)
	}

	log.Debugf("SQL: %s\n", sql)

	if _, err := tx.Exec(ctx, sql, args...); err != nil {
		return fmt.Errorf("setProcessingStateByDOPName failed: %w", err)
	}

	return tx.Commit(ctx)
}

func attr(name string, attrs []xml.Attr) (string, bool) {
	for i := range attrs {
		if attrs[i].Name.Local == name {
			return attrs[i].Value, true
		}
	}
	return "", false
}

func readFieldsFromXSDReader(r io.Reader) ([]string, error) {

	decoder := xml.NewDecoder(r)
	decoder.CharsetReader = charset.NewReaderLabel

	var fields []string

	element := xml.Name{
		Space: "http://www.w3.org/2001/XMLSchema",
		Local: "element",
	}

	for {
		tok, err := decoder.Token()
		switch {
		case tok == nil && err == io.EOF:
			return fields, nil
		case err != nil:
			return nil, err
		}
		switch t := tok.(type) {
		case xml.StartElement:
			if t.Name == element {
				if v, ok := attr("type", t.Attr); ok && v == "xs:string" {
					if v, ok := attr("name", t.Attr); ok {
						fields = append(fields, v)
					}
				}
			}
		}
	}
}

// readFieldsFromXSD parse the xsd file and extracts the metadata fields
// which later are gotten from the xml file and entered into the db.
func readFieldsFromXSD(xsdFile string) ([]string, error) {
	f, err := os.Open(xsdFile)
	if err != nil {
		return nil, fmt.Errorf("readFieldsFromXSD: %w", err)
	}
	defer f.Close()
	return readFieldsFromXSDReader(f)
}

func readValuesFromXMLReader(r io.Reader, fields []string) (map[string]string, error) {

	decoder := xml.NewDecoder(r)
	decoder.CharsetReader = charset.NewReaderLabel

	inFields := func(name string) (string, bool) {
		for _, field := range fields {
			if strings.EqualFold(name, field) {
				return field, true
			}
		}
		return "", false
	}

	result := map[string]string{}

	type consumer struct {
		b     *strings.Builder
		field string
	}

	var stack []consumer

	for {
		tok, err := decoder.Token()
		switch {
		case tok == nil && err == io.EOF:
			return result, nil
		case err != nil:
			return nil, err
		}
		switch t := tok.(type) {
		case xml.StartElement:
			field, ok := inFields(t.Name.Local)
			var b *strings.Builder
			if ok {
				b = new(strings.Builder)
			}
			stack = append(stack, consumer{b: b, field: field})
		case xml.CharData:
			if n := len(stack); n > 0 {
				if c := &stack[n-1]; c.b != nil {
					c.b.Write(t)
				}
			}
		case xml.EndElement:
			if n := len(stack); n > 0 {
				if c := &stack[n-1]; c.b != nil {
					result[c.field] = c.b.String()
				}
				stack[n-1] = consumer{}
				stack = stack[:n-1]
			}
		}
	}
}

func readValuesFromXML(xmlFile string, fields []string) (map[string]string, error) {
	f, err := os.Open(xmlFile)
	if err != nil {
		return nil, fmt.Errorf("readValuesFromXML: %w", err)
	}
	defer f.Close()
	return readValuesFromXMLReader(f, fields)
}

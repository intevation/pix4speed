// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"reflect"
	"testing"
)

func TestResolveiDeepRecursion(t *testing.T) {
	type bad struct {
		Recurse string `toml:"recurse"`
	}

	input := bad{
		Recurse: `%(recurse)s`,
	}
	if err := resolve([]reflect.Value{
		reflect.ValueOf(&input).Elem(),
	}); err == nil {
		t.Errorf("not failing is failing\n")
	}
}

func TestResolve(t *testing.T) {

	type foo struct {
		Rce string       `toml:"rce"`
		Bar string       `toml:"bar"`
		Baz string       `toml:"baz"`
		Boo parsedParams `toml:"boo"`
	}

	input := foo{
		Rce: `rce`,
		Bar: `sou%(rce)s`,
		Baz: `copy from %(bar)s is valid`,
		Boo: parsedParams{`unchanged`, `xx%(bar)sxx`, `%(unresolved)s`},
	}
	want := foo{
		Rce: `rce`,
		Bar: `source`,
		Baz: `copy from source is valid`,
		Boo: parsedParams{`unchanged`, `xxsourcexx`, `%(unresolved)s`},
	}
	have := input
	resolve([]reflect.Value{
		reflect.ValueOf(&have).Elem(),
	})

	if !reflect.DeepEqual(want, have) {
		t.Errorf("%q: want %q have %q\n", input, want, have)
	}
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"context"
	"fmt"
	"strconv"
	"strings"
	"sync"

	pgx "github.com/jackc/pgx/v5"
)

type column struct {
	name string
	typ  string
}

type columns []column

var (
	columnsCacheMu sync.Mutex
	columnsCache   = map[string]columns{}
)

const columnDataSQL = `
SELECT column_name, data_type
FROM information_schema.columns
WHERE table_schema = 'public' AND table_name = $1
ORDER BY ordinal_position`

// tableColumns returns the meta informations column names and types of a given
// table.
func tableColumns(ctx context.Context, tx pgx.Tx, table string) (columns, error) {
	columnsCacheMu.Lock()
	defer columnsCacheMu.Unlock()
	// We cache these.
	if cols := columnsCache[table]; cols != nil {
		return cols, nil
	}

	rows, err := tx.Query(ctx, columnDataSQL, table)
	if err != nil {
		return nil, err
	}
	cols, err := pgx.CollectRows(rows, func(row pgx.CollectableRow) (column, error) {
		var col column
		err := row.Scan(&col.name, &col.typ)
		return col, err
	})
	if err != nil {
		return nil, err
	}
	columnsCache[table] = cols
	return cols, nil
}

func containsEqualFold[V any](m map[string]V) func(string) (V, bool) {
	return func(key string) (V, bool) {
		for k, v := range m {
			if strings.EqualFold(k, key) {
				return v, true
			}
		}
		var v V
		return v, false
	}
}

type cast func(string) string

func (cols columns) genSQL(
	fields map[string]string,
	casts map[string]cast,
) ([]string, []string, []any) {
	var (
		inFields     = containsEqualFold(fields)
		inCasts      = containsEqualFold(casts)
		keys, values []string
		args         []any
	)
	for i := range cols {
		if v, ok := inFields(cols[i].name); ok {
			keys = append(keys, fmt.Sprintf(`"%s"`, cols[i].name))
			param := "$" + strconv.Itoa(len(values)+1)
			if cast, ok := inCasts(cols[i].name); ok {
				param = cast(param)
			} else {
				param += "::" + cols[i].typ
			}
			values = append(values, param)
			args = append(args, v)
		}
	}
	return keys, values, args
}

func (cols columns) find(name string) int {
	for i := range cols {
		if strings.EqualFold(cols[i].name, name) {
			return i
		}
	}
	return -1
}

func setValue(m map[string]string, key, value string) {
	for k := range m {
		if strings.EqualFold(k, key) {
			m[k] = value
			return
		}
	}
	m[key] = value
}

func deleteValue(m map[string]string, key string) {
	for k := range m {
		if strings.EqualFold(k, key) {
			delete(m, k)
			return
		}
	}
}

func (cols columns) updateStatement(
	table string,
	fields map[string]string,
	casts map[string]cast,
	where, value string,
) (string, []any) {

	keys, values, args := cols.genSQL(fields, casts)

	arg := func(v string) string {
		placeholder := "$" + strconv.Itoa(len(args)+1)
		args = append(args, v)
		return placeholder
	}

	whereKey := `"` + where + `"`
	var whereValue string

	if cast, ok := containsEqualFold(casts)(where); ok {
		whereValue = cast(arg(whereValue))
	} else if idx := cols.find(where); idx != -1 {
		whereValue = arg(whereValue) + "::" + cols[idx].typ
	} else {
		whereValue = arg(whereValue)
	}

	sql := fmt.Sprintf(
		`UPDATE "%s" SET (%s) = (%s) WHERE %s = %s`,
		table,
		strings.Join(keys, ", "),
		strings.Join(values, ", "),
		whereKey, whereValue)

	return sql, args
}

func (cols columns) insertStatement(
	table string,
	fields map[string]string,
	casts map[string]cast,
) (string, []any) {

	keys, values, args := cols.genSQL(fields, casts)

	sql := fmt.Sprintf(`INSERT INTO "%s" (%s) VALUES (%s)`,
		table,
		strings.Join(keys, ", "),
		strings.Join(values, ", "))

	return sql, args
}

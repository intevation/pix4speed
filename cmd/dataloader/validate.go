// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"bufio"
	"bytes"
	"errors"
	"fmt"
	"math"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"
	"strings"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

// dataValidator validates the required sources for creating
// the image pyramid and providing a meta-data layer.
// Use the validateDeliveryNote-Method for running a
// check if all data which belongs to it is valid.
type dataValidator struct {
	cfg *config
}

func newDataValidator(cfg *config) *dataValidator {
	return &dataValidator{
		cfg: cfg,
	}
}

func readLines(filename string, n int) ([]string, error) {
	f, err := os.Open(filename)
	if err != nil {
		if os.IsNotExist(err) {
			log.Errorf("Deliverynot %q does not exist.\n", filename)
		}
	}
	defer f.Close()
	var lines []string
	sc := bufio.NewScanner(f)
	for i := 0; i < n && sc.Scan(); i++ {
		lines = append(lines, sc.Text())
	}
	if err := sc.Err(); err != nil {
		return nil, err
	}
	return lines, nil
}

// validateDeliveryNote validates a delivery note.
// 1. Does the file exists?
// 2. Is it consist of a valid tiff, tfw and xml file?
func (dv *dataValidator) validateDeliveryNote(
	dataDir, deliveryNoteFile string,
) (string, string, error) {
	lines, err := readLines(deliveryNoteFile, 6)
	if err != nil {
		return "", "", err
	}

	if len(lines) < 6 {
		return "", "", fmt.Errorf("no data could be read from %q",
			deliveryNoteFile)
	}

	log.Debugln("Files to check:")
	log.Debugf("%q\n", lines)

	var tiffErr, tfwErr, xmlErr error

	var tiff, xml string

	if dv.cfg.DataLoader.ValidateTIFFFiles {
		tiff = lines[0]
		tiffErr = dv.validateTiff(filepath.Join(dataDir, tiff))
	}

	if dv.cfg.DataLoader.ValidateTFWFiles {
		tfw := lines[1]
		tfwErr = dv.validateTfw(filepath.Join(dataDir, tfw))
	}

	if dv.cfg.DataLoader.ValidateXMLFiles {
		xml = lines[2]
		mdp := newMetaDataParser(dv.cfg)
		xmlErr = mdp.validateXMLFile(filepath.Join(dataDir, xml))
	}

	var (
		tiffOk = tiffErr == nil
		tfwOk  = tfwErr == nil
		xmlOk  = xmlErr == nil
	)

	log.Infof("Is Tiff valid: %t\n", tiffOk)
	log.Infof("Is Tfw valid: %t\n", tfwOk)
	log.Infof("Is Xml valid: %t\n", xmlOk)

	if tiffOk && tfwOk && xmlOk {
		return tiff, xml, nil
	}

	return "", "", joinErrors(tiffErr, tfwErr, xmlErr)
}

// joinErrors creates a new error based on the Error
// messages from the given errors.
func joinErrors(errs ...error) error {
	var msgs []string
	for _, err := range errs {
		if err != nil {
			msgs = append(msgs, err.Error())
		}
	}
	return errors.New(strings.Join(msgs, ", "))
}

// checkFile checks if a file exists in filesystem and
// it is not a folder.
func checkFile(filename string) error {
	fi, err := os.Stat(filename)
	if err == nil {
		if m := fi.Mode(); m.IsRegular() || m&os.ModeSymlink != 0 {
			return nil
		}
		return errors.New("not a plain file")
	}
	return err
}

// validateTiff validates a tiff.
// Following checks will be done:
// * file exists
// * file is a geotiff
// * the georeference of the file match to it.
// * the groundresoultion
// * the width and height of the image
func (dv *dataValidator) validateTiff(tiff string) error {

	log.Infof("Validating tiff: %q\n", tiff)

	if err := checkFile(tiff); err != nil {
		log.Errorf("Tiff %q does not exist.\n", tiff)
		return err
	}

	originX, err := dv.extractOriginXFromFilename(tiff)
	if err != nil {
		log.Errorln("Could not extract X-Coordinate from tiff filename")
		return err
	}

	originY, err := dv.extractOriginYFromFilename(tiff)
	if err != nil {
		log.Errorln("Could not extract Y-Coordinate from tiff filename")
		return err
	}

	tmp := newTIFFMetaParser(dv.cfg)
	dataset, err := tmp.parseFile(tiff)
	if err != nil {
		return fmt.Errorf("no file found for %q: %w", tiff, err)
	}

	// XXX: The python version used hard equals.
	const eps = 1e-5
	eq := func(a, b float64) bool { return equalsEps(a, b, eps) }

	// Test if the Size of the Raster is as big as expected.
	size := float64(dv.cfg.DataValidator.RasterSize)
	if !eq(dataset.Size[0], size) || !eq(dataset.Size[1], size) {
		return errors.New("wromg raster size")
	}

	log.Debugf("Size is %f x %f\n", dataset.Size[0], dataset.Size[1])

	geotransform := dataset.GeoTransform

	// Look if the coodinates given in the name of the tiff are the
	// same as fetched from the file itself.

	if !eq(geotransform[0], originX) {
		return errors.New("wrong origin x-coordinate")
	}
	log.Debugf("Origin X = %f\n", geotransform[0])

	if !eq(geotransform[3], originY) {
		return errors.New("wrong origin y-coordinate")
	}
	log.Debugf("Origin Y = %f\n", geotransform[3])

	// Have a look at the resolution of the raster (should be 0.2).
	if !eq(geotransform[1], dv.cfg.DataValidator.PixelSize) {
		return errors.New("wrong pixel size in x")
	}
	log.Debugf("Pixel Size X = %f\n", geotransform[1])

	if !eq(geotransform[5], -dv.cfg.DataValidator.PixelSize) {
		return errors.New("wrong pixel size in y")
	}
	log.Debugf("Pixel Size X = %f\n", geotransform[5])

	return nil
}

// validateTfw checks if the syntax and the information
// in a given tfw-file is valid.
// it will be chekct if:
// * if the file exists
// * if there are 6 rows in file
// * if the georeference is valid
// * if the georeference matches to the given file
func (dv *dataValidator) validateTfw(tfw string) error {
	log.Infof("Validating tfw: %q\n", tfw)

	lines, err := readLines(tfw, 6)
	if err != nil {
		return fmt.Errorf("validateTfw failed: %w", err)
	}
	if len(lines) < 6 {
		return fmt.Errorf(
			"wrong number of lines from file. Must be 6 got %d",
			len(lines))
	}

	xCoord, xcErr := strconv.ParseFloat(lines[4], 64)
	yCoord, ycErr := strconv.ParseFloat(lines[5], 64)

	if xcErr != nil || ycErr != nil {
		return errors.New(
			"coordinate value read from tfw is not a decimal values")
	}

	xCoord, yCoord = math.Round(xCoord), math.Round(yCoord)

	log.Debugf("X-Coordinate: %f\n", xCoord)
	log.Debugf("Y-Coordinate: %f\n", yCoord)

	originX, err := dv.extractOriginXFromFilename(tfw)
	if err != nil {
		log.Errorf("Could not extract X-Coordinate from tfw-filename")
		return err
	}

	originY, err := dv.extractOriginYFromFilename(tfw)
	if err != nil {
		log.Errorf("Cound not extract Y-Coordinate from tfw-filename")
		return err
	}

	// XXX: The Python version did hard equal comparisions.
	const eps = 1e-5

	if !equalsEps(xCoord, originX, eps) || !equalsEps(yCoord, originY, eps) {
		log.Warnln("Coordinates read from tfw are not equal to" +
			" Coordinates extracted from filename")
		return errors.New("unequal coordinates")
	}

	var ps [4]float64
	for i := range ps {
		var err error
		if ps[i], err = strconv.ParseFloat(lines[i], 64); err != nil {
			log.Errorln("Pixel size values read from tfw are no decimal values")
			return err
		}
	}

	if pix := dv.cfg.DataValidator.PixelSize; !equalsEps(ps[0], pix, eps) {
		return fmt.Errorf(
			"pixelsize Line 1 is not as expected %f", pix)
	}

	if !equalsEps(ps[1], 0, eps) {
		return errors.New("pixelsize Line 2 is not as expected 0.0")
	}

	if !equalsEps(ps[2], 0, eps) {
		return errors.New("pixelsize Line 3 is not as expected 0.0")
	}

	if pix := -dv.cfg.DataValidator.PixelSize; !equalsEps(ps[3], pix, eps) {
		return fmt.Errorf(
			"pixelsize Line 4 is not as expected %f", pix)
	}

	return nil
}

// equalsEps returns true if the difference between a and b is less than eps.
func equalsEps(a, b, eps float64) bool {
	return math.Abs(a-b) < eps
}

// This method extracts the y-coordinatevalue from a given filename.
// The filename will be splitted using "_" as separator.
// the y-value in kilometer is located in the third fragment.
func (dv *dataValidator) extractOriginYFromFilename(filename string) (float64, error) {
	base := filepath.Base(filename)
	fragments := strings.Split(base, "_")
	if len(fragments) < 3 {
		return 0, errors.New("too less fragments")
	}
	originY, err := strconv.ParseFloat(fragments[2], 64)
	if err != nil {
		return 0, err
	}
	// XXX: swap found in Python original.
	resolution := dv.cfg.DataValidator.PixelSize
	pixel := float64(dv.cfg.DataValidator.RasterSize)
	originY *= float64(dv.cfg.DataLoader.scale())
	originY += resolution * pixel
	log.Debugf("Origin Y from Filename = %f\n", originY)
	return originY, nil
}

// extractOriginXFromFilename extracts the x-coordinatevalue from
// the given filename.
// The filename will be splitted using "_" as separator.
// The x-value in kilometer is located in the second fragment.
func (dv *dataValidator) extractOriginXFromFilename(filename string) (float64, error) {
	base := filepath.Base(filename)
	fragments := strings.Split(base, "_")
	if len(fragments) < 2 {
		return 0, errors.New("too less fragments")
	}
	originX, err := strconv.ParseFloat(fragments[1], 64)
	if err != nil {
		return 0, err
	}
	ofs := dv.cfg.DataLoader.ProjXOffset
	originX = (originX - ofs) * float64(dv.cfg.DataLoader.scale())
	log.Debugf("Origin X from filename = %f\n", originX)
	return originX, nil
}

// ValidationError is returned as an error from validateXMLFile.
type ValidationError string

// Error implements the error interface.
func (ve ValidationError) Error() string {
	return string(ve)
}

// validateXMLFile validates a given xml file with xsd schema.
func (mdp *metaDataParser) validateXMLFile(xmlFile string) error {

	linterPath := "xmllint"

	if path := mdp.cfg.MetaParser.XMLLint; path != "" {
		linterPath = path
	}

	cmd := exec.Command(
		linterPath,
		"--schema", mdp.cfg.MetaParser.XSDFileName,
		"--noout",
		xmlFile)

	var stderr bytes.Buffer
	cmd.Stderr = &stderr

	if err := cmd.Start(); err != nil {
		return err
	}

	if err := cmd.Wait(); err != nil {
		var exitErr *exec.ExitError
		if errors.As(err, &exitErr) {
			if !exitErr.Success() {
				return ValidationError(stderr.String())
			}
		}
		return err
	}
	return nil
}

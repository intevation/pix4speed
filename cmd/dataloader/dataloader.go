// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"bufio"
	"errors"
	"fmt"
	"io/fs"
	"os"
	"path/filepath"
	"sort"
	"strings"
	"sync"
	"time"

	"heptapod.host/intevation/pix4speed/pkg/log"
)

// dataLoader is the central element of the update and insert process
// of the image pyramid.
// It provides methods for managing the update and insert process.
// It also manages the initialisation of a new image pramid and the
// inital import of the metadata.
// It provides a method to determine all delivery note files and
// set and reset the emergency stop file.
// If an emergency stop file is set it is not allowed to run updates
// during the time which is defined in it.
// The dataLoader also provides the logic to import a list of
// delivery notes into the pyramid. This can be used e.g. by cronjobs.
type dataLoader struct {
	cfg *config
}

// newDataLoader creates a new data loader.
func newDataLoader(cfg *config) *dataLoader {
	return &dataLoader{
		cfg: cfg,
	}
}

// loadDataFromDeliveryNote imports the data which is referenced
// by a deliverynote into the pyramid and into the metadata-table.
// If flag "processFile" is set to false the data will not e
// processed but the deliverynot moved into the folder where it will be
// detected by the cron.
func (dl *dataLoader) loadDataFromDeliveryNote(deliveryNoteFile string) error {
	log.Debugf("loadDataFromDeliveryNote has been called for %q\n",
		deliveryNoteFile)
	log.Debugf("process file: %t\n", dl.cfg.DataLoader.ProcessFile)

	if dl.cfg.DataLoader.ProcessFile {
		if err := dl.writePIDFile(); err != nil {
			log.Infoln("Cannot run because pid file already exists.")
			return err
		}
		defer dl.deletePIDFile()
		return dl.loadData(
			deliveryNoteFile,
			dl.cfg.DataLoader.DeliveryNoteFolder)
	}

	return dl.activateDeliveryNote(deliveryNoteFile)
}

// activateDeliveryNote moves the delivery note into folder for cron job.
func (dl *dataLoader) activateDeliveryNote(deliveryNoteFileName string) error {
	log.Debugf("activateDeliveryNote has been called for %q\n",
		deliveryNoteFileName)
	err := os.Rename(
		filepath.Join(dl.cfg.DataLoader.DeliveryNoteFolder, deliveryNoteFileName),
		filepath.Join(dl.cfg.DataLoader.BulkDeliveryNoteFolder, deliveryNoteFileName))
	if err != nil {
		err = fmt.Errorf("activateDeliveryNote: %w", err)
	}
	return err
}

// getAllAvailableDeliveryNotes is getting a list of all avaliable deliverynotes
// which can be processed using the webinterface.
func (dl *dataLoader) getAllAvailableDeliveryNotes() ([]string, error) {
	log.Debugln("getAllAvailableDeliveryNotes has been called")
	return dl.getDeliveryNotes()
}

// clearEmergencyStop removes the emergencystopfile.
func (dl *dataLoader) clearEmergencyStop() error {
	log.Debugln("clearEmergencyStop has been called")
	file := dl.cfg.DataLoader.EmergencyStopFile
	_, err := os.Stat(file)
	if err == nil {
		return os.Remove(file)
	}
	if errors.Is(err, fs.ErrNotExist) {
		log.Debugln("No EmergencyStopFile exists")
		err = nil
	}
	return err
}

// writeEmergencyStopFile is writing the emergency stop file.
// The given start- and end time will be written into it.
func (dl *dataLoader) writeEmergencyStopFile(startTime, endTime time.Time) error {
	log.Debugln("writeEmergencyStopFile has been called")
	file := dl.cfg.DataLoader.EmergencyStopFile
	log.Debugf("emergencystopfile: %q\n", file)
	if startTime.After(endTime) {
		return errors.New("start date is bigger than end date")
	}
	f, err := os.Create(file)
	if err != nil {
		return fmt.Errorf("writeEmergencyStopFile: %w", err)
	}
	fmt.Fprintf(f, "%s\n%s\n",
		startTime.Format(timeFormat),
		endTime.Format(timeFormat))
	if err = f.Close(); err != nil {
		err = fmt.Errorf("writeEmergencyStopFile: %w", err)
	}
	return err
}

// writePIDFile writes a pid file.
func (dl *dataLoader) writePIDFile() error {
	file := dl.cfg.DataLoader.PIDFile
	dir := filepath.Dir(file)
	if err := os.MkdirAll(dir, 0750); err != nil {
		return err
	}
	f, err := os.OpenFile(file, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0640)
	if err != nil {
		return err
	}
	fmt.Fprintf(f, "%d\n", os.Getpid())
	return f.Close()
}

// delete deletes a pid file.
// The pid file can only be deleted if the pid of the current process
// matches the pid stored in the file.
func (dl *dataLoader) deletePIDFile() error {
	f, err := os.OpenFile(dl.cfg.DataLoader.PIDFile, os.O_RDONLY, 0640)
	if err != nil {
		return err
	}
	var pid int
	n, err := fmt.Fscanf(f, "%d\n", &pid)
	f.Close()
	if err != nil {
		return err
	}
	if n != 1 {
		return errors.New("no pid found")
	}
	if pid == os.Getpid() {
		log.Debugln("pid file was written by current process -> remove it.")
		return os.Remove(dl.cfg.DataLoader.PIDFile)
	}
	return nil
}

// bulkLoad runs a bulk load (cron). This will process all
// delivery notes which are located in the configured folder.
func (dl *dataLoader) bulkLoad() error {
	log.Debugln("bulkLoad has been called")

	if _, err := os.Stat(dl.cfg.DataLoader.PIDFile); err != nil {
		if os.IsNotExist(err) {
			if err := dl.writePIDFile(); err != nil {
				return fmt.Errorf("cannot create pid file: %w", err)
			}
		} else {
			return fmt.Errorf("stating pid file failed: %w", err)
		}
	} else {
		return errors.New("pid file already exists")
	}
	defer func() {
		log.Debugln("Job finished removing pid file.")
		if err := dl.deletePIDFile(); err != nil {
			log.Errorf("deleting pid file failed: %v", err)
		}
	}()
	return dl.doBulkLoad()
}

func (dl *dataLoader) doBulkLoad() error {
	// Check if no emergency stop is defined.
	if !dl.runAllowed() {
		log.Debugln("No runs allowed at this time")
		return errors.New("no runs allowed at this time")
	}
	log.Debugln("Run is allowed and starts now...")

	// Select all deliverynotes.
	files, err := dl.getDeliveryNotes()
	if err != nil {
		return fmt.Errorf("getDeliveryNotes failed: %w", err)
	}

	if len(files) == 0 {
		log.Debugln("No delivery notes to process. Run terminates.")
		return nil
	}

	jobCh := make(chan string)
	errCh := make(chan error)
	done := make(chan struct{})

	var errs []error

	go func() {
		defer close(done)
		for err := range errCh {
			errs = append(errs, err)
		}
	}()

	var wg sync.WaitGroup
	for i, n := 0, dl.cfg.DataLoader.threadsCount(); i < n; i++ {
		wg.Add(1)
		go dl.deliveryWorker(&wg, jobCh, errCh)
	}

	for _, file := range files {
		jobCh <- file
	}

	close(jobCh)

	wg.Wait()

	<-done

	if len(errs) == 0 {
		return nil
	}

	for _, err := range errs {
		log.Errorf("doBulkLoad failed: %v\n", err)
	}

	return fmt.Errorf("doBulkLoad failed with %d errors", len(errs))
}

func (dl *dataLoader) deliveryWorker(
	wg *sync.WaitGroup,
	jobCh <-chan string,
	errCh chan<- error,
) {
	defer wg.Done()

	ignore := false

	folder := dl.cfg.DataLoader.BulkDeliveryNoteFolder

	for file := range jobCh {
		if ignore {
			continue
		}
		if !dl.runAllowed() {
			ignore = true
			continue
		}
		log.Debugf("Working on file %q.\n", file)
		if err := dl.loadData(file, folder); err != nil {
			errCh <- err
		}
	}
}

// loadData provides the logic to insert the data into the
// the pyramid and the metadata table.
// 1. Validate data
// 2. Update or insert metadata
// 3. Update pyramid
// 4. Write status and endtime to metadata
// 5. Move deliverynote into folder of processed deliverynotes
func (dl *dataLoader) loadData(deliveryNoteFile, folder string) error {
	log.Debugln("loadData has been called")

	log.Infof("Validate deliverynote %q\n", deliveryNoteFile)

	dv := newDataValidator(dl.cfg)

	tiff, xml, err := dv.validateDeliveryNote(
		dl.cfg.Default.DataFolder,
		filepath.Join(folder, deliveryNoteFile))

	if err != nil {
		log.Errorf("Data in delivery note %q is invalid.\n",
			deliveryNoteFile)
		return err
	}

	log.Infoln("Data is valid, data can be processed")

	metaDataFileName := filepath.Join(
		dl.cfg.Default.DataFolder, xml)

	log.Infof("Load metadata into db from file %q\n", metaDataFileName)

	// Insert or update the metadata into metadata table.
	mdp := newMetaDataParser(dl.cfg)

	var parseMDsuccessful error

	if !dl.cfg.DataLoader.ValidateXMLFiles {

		if dl.cfg.DataLoader.UseSimpleMetaData {
			log.Infoln("Insert short metadata")
			parseMDsuccessful = mdp.insertShortMetaData(tiff)
		} else {
			log.Infoln("Skipping parseMetaData")
		}
	} else {
		parseMDsuccessful = mdp.parseMetaData(metaDataFileName)
	}

	if parseMDsuccessful == nil {
		log.Debugln("Load metadata was successful")
	} else {
		log.Errorf("Load metadata failed for file %q: %v\n",
			metaDataFileName, parseMDsuccessful)

		if err := mdp.setProcessingStateByDOPName(
			false,
			filepath.Join(dl.cfg.Default.DataFolder, tiff),
		); err != nil {
			return fmt.Errorf("updating processing state failed: %w", err)
		}
		return parseMDsuccessful
	}

	log.Infof("Generalize %q\n", tiff)

	// Update pyramid
	rg := newRasterGeneralizer(dl.cfg)
	updateErr := rg.updateAll(tiff)

	if err := mdp.setProcessingStateByDOPName(
		updateErr == nil,
		filepath.Join(dl.cfg.Default.DataFolder, tiff),
	); err != nil {
		return fmt.Errorf("setProcessingStateByDOPName failed: %w", err)
	}

	// Move deliverynote into folder of processed deliverynotes.
	if updateErr == nil {
		if err := dl.moveDeliveryNote(deliveryNoteFile, folder); err != nil {
			return fmt.Errorf("moveDeliveryNote failed: %w", err)
		}
	}

	return updateErr
}

// moveDeliveryNote moves a deliverynote into
// folder of processed deliverynotes.
func (dl *dataLoader) moveDeliveryNote(
	deliveryNoteFileName, folder string,
) error {
	log.Debugf("moveDeliveryNote has been called for %q\n",
		deliveryNoteFileName)

	from := filepath.Join(
		folder,
		deliveryNoteFileName)
	to := filepath.Join(
		dl.cfg.DataLoader.DeliveryNoteFolderProcessed,
		deliveryNoteFileName)

	log.Debugf("%q\n", from)
	log.Debugf("%q\n", to)

	return os.Rename(from, to)
}

// allFiles gets a list of all data files are located in a given folder.
func allFiles(folder, suffix string) ([]string, error) {
	return filepath.Glob(filepath.Join(folder, "*"+suffix))
}

// getDeliveryNotes gets a list of all delivery notes.
func (dl *dataLoader) getDeliveryNotes() ([]string, error) {
	return allFiles(dl.cfg.DataLoader.DeliveryNoteFolder, ".txt")
}

const timeFormat = "2006-01-02 15:04:05"

func parseTime(s string) (time.Time, error) {
	return time.Parse(timeFormat, s)
}

// readEmergencyStopFile reads the timestamps which are stored in
// the emergency stop file.
func (dl *dataLoader) readEmergencyStopFile() ([]time.Time, error) {
	f, err := os.Open(dl.cfg.DataLoader.EmergencyStopFile)
	if err != nil {
		if os.IsNotExist(err) {
			return nil, nil
		}
		return nil, err
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	times := make([]time.Time, 0, 2)
	for len(times) < 2 && scanner.Scan() {
		line := scanner.Text()
		t, err := parseTime(line)
		if err != nil {
			log.Warnf("emergency stop file: %v\n", err)
		} else {
			times = append(times, t)
		}
	}
	if err := scanner.Err(); err != nil {
		return nil, err
	}
	sort.Slice(times, func(i, j int) bool {
		return times[i].Before(times[j])
	})
	return times, nil
}

// runAllows checks if emergency stop is set.
func (dl *dataLoader) runAllowed() bool {
	times, err := dl.readEmergencyStopFile()
	if err != nil {
		log.Warnf("runAllowed: %v.\n", err)
		return true
	}
	if len(times) < 2 {
		return true
	}
	now := time.Now()
	return now.Before(times[0]) || now.After(times[1])
}

func (dl *dataLoader) saveSimpleMetaData() error {
	log.Debugln("saveSimpleMetaData has been called")
	tiffFiles, err := allFiles(dl.cfg.Default.DataFolder, ".tif")
	if err != nil {
		return err
	}
	var failures []error
	mdp := newMetaDataParser(dl.cfg)
	for _, tiff := range tiffFiles {
		if err := mdp.insertShortMetaData(tiff); err != nil {
			failures = append(failures, err)
		}
	}
	if len(failures) == 0 {
		return nil
	}
	for _, err := range failures {
		log.Errorf("saveSimpleMetaData failed: %v\n", err)
	}
	return fmt.Errorf(
		"saveSimpleMetaData failed with %d errors", len(failures))
}

// loadMetaDataFiles loads all metadata files into database.
func (dl *dataLoader) loadMetaDataFiles() error {
	log.Debugln("loadMetaDataFiles has been called.")
	metadataFiles, err := allFiles(dl.cfg.Default.DataFolder, ".xml")
	if err != nil {
		return err
	}
	var failures []error
	for _, metadataFile := range metadataFiles {
		if err := dl.loadMetaDataFile(metadataFile); err != nil {
			log.Warnf("Metadata for file %q could not be parsed.\n", metadataFile)
			failures = append(failures, err)
		}
	}
	if len(failures) == 0 {
		return nil
	}
	for _, err := range failures {
		log.Errorf("loadMetaDataFiles failed: %v\n", err)
	}
	return fmt.Errorf(
		"loadMetaDataFiles failed with %d errors", len(failures))
}

// loadMetaDataFile parses a given metadata file.
func (dl *dataLoader) loadMetaDataFile(xmlFile string) error {
	log.Debugf("loadMetaDataFile has been called %q\n", xmlFile)

	mdp := newMetaDataParser(dl.cfg)

	if !dl.cfg.DataLoader.ValidateXMLFiles {
		log.Debugf("Not validating XML file %q\n", xmlFile)
	} else {
		if err := mdp.validateXMLFile(xmlFile); err != nil {
			log.Errorf("Failed to validate XML file %q\n", xmlFile)
			return err
		}
	}

	var err error
	if !dl.cfg.DataLoader.ValidateXMLFiles {
		log.Infoln("Skipping parseMetaData")
	} else {
		err = mdp.parseMetaData(xmlFile)
	}

	if err2 := mdp.setProcessingStateByDOPName(
		err == nil,
		strings.TrimSuffix(xmlFile, ".xml")+dl.cfg.DataLoader.XMLTIFExtraChars+".tif",
	); err2 != nil {
		if err == nil {
			err = fmt.Errorf("setProcessingStateByDOPName failed: %w", err2)
		} else {
			log.Errorf("setProcessingStateByDOPName failed: %v\n", err2)
		}
	}

	return err
}

// initPyramid initalizes a new image pyramid,
func (dl *dataLoader) initPyramid() error {
	log.Debugln("initPyramid has been called")

	log.Debugln("Loading metadata into database")

	var handleMetadata func() error

	if dl.cfg.DataLoader.UseSimpleMetaData {
		handleMetadata = dl.saveSimpleMetaData
	} else {
		handleMetadata = dl.loadMetaDataFiles
	}

	if err := handleMetadata(); err != nil {
		log.Errorln("initPyramid failed. System will exit")
		return fmt.Errorf("initPyramid: %w", err)
	}

	log.Infoln("All data for creating the pyramid are valid.")

	log.Debugln("Create pyramid.")
	rg := newRasterGeneralizer(dl.cfg)
	return rg.calculateAll()
}

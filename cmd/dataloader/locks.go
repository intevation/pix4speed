// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"sort"
	"sync"
)

type locks[K comparable] struct {
	Less func(a, b K) bool
	mu   sync.Mutex
	l    map[K]*sync.Mutex
}

func (l *locks[K]) lock(fn func() error, keys ...K) error {
	if l.Less != nil {
		sort.Slice(keys, func(i, j int) bool {
			return l.Less(keys[i], keys[j])
		})
	}
	mus := make([]*sync.Mutex, len(keys))
	l.mu.Lock()
	if l.l == nil {
		l.l = make(map[K]*sync.Mutex)
	}
	for i, k := range keys {
		m := l.l[k]
		if m == nil {
			m = new(sync.Mutex)
			l.l[k] = m
		}
		mus[i] = m
	}
	l.mu.Unlock()
	for _, m := range mus {
		m.Lock()
	}
	defer func() {
		for _, m := range mus {
			m.Unlock()
		}
	}()
	return fn()
}

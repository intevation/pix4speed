// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
)

// tiffMetaParser parses meta informations from tiff files.
type tiffMetaParser struct {
	cfg *config
}

// tiffMeta is the meta information extracted with the tiffMetaParser.
type tiffMeta struct {
	Size         []float64 `json:"size"`
	GeoTransform []float64 `json:"geoTransform"`
}

// newTIFFMetaParser creates a new tiffMetaParser.
func newTIFFMetaParser(cfg *config) *tiffMetaParser {
	return &tiffMetaParser{
		cfg: cfg,
	}
}

// parseFile extracts meta informations from a given tiff file-
func (tmp *tiffMetaParser) parseFile(tiff string) (*tiffMeta, error) {

	gdalinfo := tmp.cfg.RasterGeneralizer.gdalinfo()

	cmd := exec.Command(gdalinfo, "-json", tiff)

	var (
		stdout bytes.Buffer
		stderr bytes.Buffer
	)

	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	if err := cmd.Start(); err != nil {
		return nil, err
	}

	if err := cmd.Wait(); err != nil {
		var exitErr *exec.ExitError
		if errors.As(err, &exitErr) {
			if !exitErr.Success() {
				return nil, fmt.Errorf("parseFile failed: %s", stderr.String())
			}
		}
		return nil, err
	}

	var meta tiffMeta

	if err := json.Unmarshal(stdout.Bytes(), &meta); err != nil {
		return nil, fmt.Errorf("parsinh json failed: %w", err)
	}

	if len(meta.Size) < 2 {
		return nil, errors.New("parseFile: Size too small")
	}

	if len(meta.GeoTransform) < 6 {
		return nil, errors.New("parseFile: GeoTransform too small")
	}

	return &meta, nil
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

package main

import (
	"reflect"
	"strings"
	"testing"
)

const demoXML = `<?xml version="1.0" encoding="UTF-8"?>
<metadata xml:lang="de">
  <metadaten>
	<land>Wonderland</land>
	<eigentuemer>Alice</eigentuemer>
	<kachelname>Rabbithole</kachelname>
	<sensor>Goldeneye</sensor>
	<bodenpixelgroesse>20</bodenpixelgroesse>
	<spektralkanaele>RGB</spektralkanaele>
	<bezugsflaeche>Bezugsland</bezugsflaeche>
	<anzahl_spalten>100<nested/>00</anzahl_spalten>
	<anzahl_zeilen>10000</anzahl_zeilen>
	<standardabweichung>40</standardabweichung>
	<dateiformat>TIF</dateiformat>
	<kompression>0</kompression>
  </metadaten>
</metadata>`

const demoXSD = `<?xml version="1.0" encoding="UTF-8"?>
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" elementFormDefault="qualified">
  <xs:element name="Metadaten">
    <xs:complexType>
      <xs:sequence>
        <xs:element name="EIGENTUEMER" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="KACHELNAME" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="BILDFLUGDATUM" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="SENSOR" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="BODENPIXELGROESSE" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="SPEKTRALKANAELE" type="xs:string" minOccurs="1" maxOccurs="1" /> <!-- Year-Month -->
        <xs:element name="BEZUGSSYSTEM" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="BEZUGSFLAECHE" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="KOORDINATENURSPRUNG_RECHTSWERT" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="KOORDINATENURSPRUNG_HOCHWERT" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="ANZAHL_SPALTEN" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="ANZAHL_ZEILEN" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="STANDARDABWEICHUNG" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="DATEIFORMAT" type="xs:string" minOccurs="1" maxOccurs="1" /> <!-- Date German Notation -->
        <xs:element name="HINTERGRUNDFARBE" type="xs:string" minOccurs="1" maxOccurs="1" />
        <xs:element name="KOMPRESSION" type="xs:string" minOccurs="1" maxOccurs="1" /> <!-- Year-Month_Day -->
        <xs:element name="VERSION_REGELWERK" type="xs:string" minOccurs="1" maxOccurs="1" />
      </xs:sequence>
    </xs:complexType>
  </xs:element>
</xs:schema>`

func TestReadValuesFromXMLReader(t *testing.T) {
	fields := []string{
		"LAND",
		"EIGENTUEMER",
		"KACHELNAME",
		"BILDFLUGDATUM",
		"SENSOR",
		"BODENPIXELGROESSE",
		"SPEKTRALKANAELE",
		"BEZUGSSYSTEM",
		"BEZUGSFLAECHE",
		"KOORDINATENURSPRUNG_RECHTSWERT",
		"KOORDINATENURSPRUNG_HOCHWERT",
		"ANZAHL_SPALTEN",
		"ANZAHL_ZEILEN",
		"STANDARDABWEICHUNG",
		"DATEIFORMAT",
		"HINTERGRUNDFARBE",
		"KOMPRESSION",
		"VERSION_REGELWERK",
	}

	want := map[string]string{
		"LAND":               "Wonderland",
		"EIGENTUEMER":        "Alice",
		"KACHELNAME":         "Rabbithole",
		"SENSOR":             "Goldeneye",
		"BODENPIXELGROESSE":  "20",
		"SPEKTRALKANAELE":    "RGB",
		"BEZUGSFLAECHE":      "Bezugsland",
		"ANZAHL_SPALTEN":     "10000",
		"ANZAHL_ZEILEN":      "10000",
		"STANDARDABWEICHUNG": "40",
		"DATEIFORMAT":        "TIF",
		"KOMPRESSION":        "0",
	}

	reader := strings.NewReader(demoXML)

	have, err := readValuesFromXMLReader(reader, fields)
	if err != nil {
		t.Errorf("error not expected: %v", err)
	}

	if !reflect.DeepEqual(have, want) {
		t.Errorf("want does not match have: %+q %+q\n", want, have)
	}
}

func TestReadFieldsFromXSDReaderTest(t *testing.T) {

	want := []string{
		"EIGENTUEMER",
		"KACHELNAME",
		"BILDFLUGDATUM",
		"SENSOR",
		"BODENPIXELGROESSE",
		"SPEKTRALKANAELE",
		"BEZUGSSYSTEM",
		"BEZUGSFLAECHE",
		"KOORDINATENURSPRUNG_RECHTSWERT",
		"KOORDINATENURSPRUNG_HOCHWERT",
		"ANZAHL_SPALTEN",
		"ANZAHL_ZEILEN",
		"STANDARDABWEICHUNG",
		"DATEIFORMAT",
		"HINTERGRUNDFARBE",
		"KOMPRESSION",
		"VERSION_REGELWERK",
	}

	reader := strings.NewReader(demoXSD)

	have, err := readFieldsFromXSDReader(reader)
	if err != nil {
		t.Errorf("error not expected: %v", err)
	}

	if !reflect.DeepEqual(have, want) {
		t.Errorf("want does not match have: %q\n", have)
	}
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Package main implements the dataloader tool for px4speed.
package main

import "heptapod.host/intevation/pix4speed/pkg/log"

const defaultProperties = "default.properties"

func check(err error) {
	if err != nil {
		log.Fatalf("error: %v\n", err)
	}
}

func main() {
	if isCGI() {
		runAsCGI()
	} else {
		runAsCommandLine()
	}
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Package main implements a batch runner for gdalwarp commands.
package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"log"

	"github.com/google/shlex"
)

const defaultGdalwarpCmdFile = "gdalwarp_cmd.txt"

func processing(
	inputFiles []string,
	outputFolder, gdalCmd, enlargement string,
	dryRun bool,
) error {
	log.Printf("info: processing %s\n", strings.Join(inputFiles, ", "))

	okCount := 0

	args, err := shlex.Split(gdalCmd)
	if err != nil {
		return err
	}

	if len(args) == 0 {
		return errors.New("no command to execute")
	}

	for _, f := range inputFiles {
		basename := filepath.Base(f)
		ext := filepath.Ext(basename)
		outname := strings.TrimSuffix(basename, ext) + enlargement + ".tif"
		output := filepath.Join(outputFolder, outname)

		call := append(append([]string(nil), args...), f, output)

		if dryRun {
			fmt.Println(strings.Join(call, " "))
			okCount++
			continue
		}

		cmd := exec.Command(call[0], call[1:]...)
		if err := cmd.Run(); err != nil {
			log.Printf("error: %v\n", err)
		} else {
			log.Println("OK")
			okCount++
		}
	}

	if okCount == 0 {
		return errors.New("no file could be transformed")
	}
	log.Printf(
		"info: Summary transform: %d/%d\n", okCount, len(inputFiles))

	return nil
}

func getGdalwarpCommand(filename, name string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	var known []string
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if len(line) == 0 || strings.HasPrefix(line, "#") {
			continue
		}
		cmdName, cmd, ok := strings.Cut(line, "=")
		if !ok {
			continue
		}
		if cmdName = strings.TrimSpace(cmdName); cmdName == name {
			return strings.TrimSpace(cmd), nil
		}
		known = append(known, cmdName)
	}
	if err := sc.Err(); err != nil {
		return "", err
	}
	return "", fmt.Errorf(
		"cannot find command %q in file %q. known commands: %s",
		name, filename, strings.Join(known, ", "))
}

func stringVar(long, short, value, usage string) *string {
	p := flag.String(long, value, usage)
	flag.StringVar(p, short, value, usage+" (shorthand)")
	return p
}

func boolVar(long, short string, value bool, usage string) *bool {
	p := flag.Bool(long, value, usage)
	flag.BoolVar(p, short, value, usage+" (shorthand)")
	return p
}

func isFile(filename string) bool {
	fi, err := os.Stat(filename)
	if err != nil {
		log.Printf("error: %v\n", err)
		return false
	}
	return fi.Mode().IsRegular()
}

func isDir(dirname string) bool {
	di, err := os.Stat(dirname)
	if err != nil {
		log.Printf("error: %v\n", err)
		return false
	}
	return di.IsDir()
}

func main() {
	var (
		nameAdd = stringVar(
			"name_add", "n", "",
			"Enlarge the output filename between basename and extension")
		gdalCmdName = stringVar(
			"gdal_cmd_name", "c", "",
			"Set the name of the gdalwarp command")
		gdalCmdFile = stringVar(
			"gdal_cmd_file", "f",
			defaultGdalwarpCmdFile,
			"use another gdalwarp command file "+
				"(in this file per line: "+
				"cmd_name=gdalwarp ....)")
		dryRun = boolVar(
			"dry_run", "d", false,
			"test run (just print the commands)")
	)

	flag.Parse()

	if flag.NArg() < 2 {
		log.Println("Missing arguments")
		os.Exit(1)
	}

	if fn := *gdalCmdFile; !isFile(fn) {
		log.Printf(
			"error: cannot open gdalwarp cmd file %q\n", fn)
		os.Exit(2)
	}

	if dn := flag.Arg(flag.NArg() - 1); !isDir(dn) {
		log.Println("error: output folder cannot be opened " +
			"(no folder or not existing)")
		os.Exit(5)
	}

	for _, fn := range flag.Args()[:flag.NArg()-1] {
		if !isFile(fn) {
			log.Printf("error: cannot open input file %q", fn)
			os.Exit(6)
		}
	}

	if *gdalCmdName == "" {
		log.Println("error: no gdalwarp command nane is given (use -c)")
		os.Exit(4)
	}

	cmd, err := getGdalwarpCommand(*gdalCmdFile, *gdalCmdName)
	if err != nil {
		log.Printf("error: %v\n", err)
		os.Exit(3)
	}

	if err := processing(
		flag.Args()[:flag.NArg()-1],
		flag.Arg(flag.NArg()-1),
		cmd, *nameAdd, *dryRun,
	); err != nil {
		log.Printf("error: %v\n", err)
		os.Exit(5)
	}
}

// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

// Package main implements the command to generate the grid.
package main

import (
	"bufio"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/google/shlex"
	shp "github.com/jonas-p/go-shp"
)

const gdalwarpCmdFile = "gdalwarp_cmd_grid.txt"

type row struct {
	gridName   string
	inputFile  string
	xMin       string
	yMin       string
	xMax       string
	yMax       string
	outputFile string
}

func intersects(a, b *shp.Box) bool {
	return !(b.MinX > a.MaxX || a.MinX > b.MaxX ||
		b.MinY > a.MaxY || a.MinY > b.MaxY)
}

func processShapes(
	shpname1, shpname2,
	prefix, postfix string,
) ([]row, error) {

	log.Println("info: Processing shapefiles")

	shp1, err := shp.Open(shpname1)
	if err != nil {
		return nil, fmt.Errorf("cannot open %q: %w", shpname1, err)
	}
	defer shp1.Close()

	shp2, err := shp.Open(shpname1)
	if err != nil {
		return nil, fmt.Errorf("cannot open %q: %w", shpname2, err)
	}
	defer shp2.Close()

	fieldIndex := func(name string, fields []shp.Field) int {
		if err != nil {
			return -1
		}
		for i, f := range fields {
			if f.String() == name {
				return i
			}
		}
		err = fmt.Errorf("missing field %q in shape", name)
		return -1
	}

	fields1 := shp1.Fields()
	fields2 := shp2.Fields()

	var (
		xminYminIdx = fieldIndex("XMIN_YMIN", fields1)
		xminIdx     = fieldIndex("XMIN", fields1)
		yminIdx     = fieldIndex("YMIN", fields1)
		xmaxIdx     = fieldIndex("XMAX", fields1)
		ymaxIdx     = fieldIndex("YMAX", fields1)
		locationIdx = fieldIndex("location", fields2)
	)

	if err != nil {
		return nil, err
	}

	var count int
	var result []row

	for shp1.Next() {
		n1, p1 := shp1.Shape()
		bbox1 := p1.BBox()

		var (
			xminYmin = shp1.ReadAttribute(n1, xminYminIdx)
			xmin     = shp1.ReadAttribute(n1, xminIdx)
			ymin     = shp1.ReadAttribute(n1, yminIdx)
			xmax     = shp1.ReadAttribute(n1, xmaxIdx)
			ymax     = shp1.ReadAttribute(n1, ymaxIdx)
		)

		for shp2.Next() {
			n2, p2 := shp2.Shape()
			count++

			bbox2 := p2.BBox()
			if !intersects(&bbox1, &bbox2) {
				continue
			}
			// TODO: Check if we really have to intersect the geometries!?
			location := shp2.ReadAttribute(n2, locationIdx)

			nameOutput := prefix + xminYmin + postfix
			result = append(result, row{
				gridName:   xminYmin,
				inputFile:  location,
				xMin:       xmin,
				yMin:       ymin,
				xMax:       xmax,
				yMax:       ymax,
				outputFile: nameOutput,
			})

		}
	}

	log.Printf("Intersecting complete %d/%d (intersects/all)\n",
		len(result), count)

	return result, nil
}

func process(
	rows []row,
	sourceDir, destDir,
	gdalCmd, outputFile string,
	dryRun bool,
) error {
	args, err := shlex.Split(gdalCmd)
	if err != nil {
		return fmt.Errorf("process: %w", err)
	}
	if len(args) == 0 {
		return errors.New("process: not a valid command")
	}

	var out io.WriteCloser

	if outputFile != "" {
		log.Printf(
			"info: Write debug2 output to %q\n", outputFile)
		var err error
		if out, err = os.Create(outputFile); err != nil {
			return fmt.Errorf("process: %w", err)
		}
	}

	var (
		input   []string
		current *row
	)

	callCommand := func() error {
		err := processCommand(args, current, input, destDir, out, dryRun)
		if err != nil && out != nil {
			out.Close()
		}
		return err
	}

	for i := range rows {
		row := &rows[i]
		if current == nil {
			current = row
		}
		if current.gridName == row.gridName {
			input = append(input, filepath.Join(sourceDir, row.inputFile))
		} else {
			if err := callCommand(); err != nil {
				return fmt.Errorf("process: %w", err)
			}
			input = append(input[:0], filepath.Join(sourceDir, row.inputFile))
			current = row
		}
	}
	if len(input) > 0 {
		if err := callCommand(); err != nil {
			return fmt.Errorf("process: %w", err)
		}
	}

	if out != nil {
		return out.Close()
	}

	return errors.New("process: not implemented, yet")
}

func processCommand(
	args []string,
	current *row,
	input []string,
	destDir string,
	out io.Writer,
	dryRun bool,
) error {
	args = append(append(append(
		append([]string(nil), args...),
		current.xMin, current.yMin,
		current.xMax, current.yMax),
		input...),
		filepath.Join(destDir, current.outputFile),
	)
	if out != nil || dryRun {
		line := strings.Join(args, " ")
		if out != nil {
			if _, err := fmt.Fprintln(out, line); err != nil {
				return fmt.Errorf("processCommand: %w", err)
			}
		}
		if dryRun {
			fmt.Println(line)
			return nil
		}
	}
	cmd := exec.Command(args[0], args[1:]...)
	if err := cmd.Run(); err != nil {
		return fmt.Errorf("processCommand: %w", err)
	}
	log.Println("info: OK")
	return nil
}

func getGdalwarpCommand(filename, name string) (string, error) {
	f, err := os.Open(filename)
	if err != nil {
		return "", err
	}
	defer f.Close()
	sc := bufio.NewScanner(f)
	var known []string
	for sc.Scan() {
		line := strings.TrimSpace(sc.Text())
		if len(line) == 0 || strings.HasPrefix(line, "#") {
			continue
		}
		cmdName, cmd, ok := strings.Cut(line, "=")
		if !ok {
			continue
		}
		if cmdName = strings.TrimSpace(cmdName); cmdName == name {
			return strings.TrimSpace(cmd), nil
		}
		known = append(known, cmdName)
	}
	if err := sc.Err(); err != nil {
		return "", err
	}
	return "", fmt.Errorf(
		"cannot find command %q in file %q. known commands: %s",
		name, filename, strings.Join(known, ", "))
}

func writeRows(rows []row, w io.Writer) {
	for i := range rows {
		row := &rows[i]
		fmt.Fprintf(w, "%s;%s;%s;%s;%s;%s;%s\n",
			row.gridName,
			row.inputFile,
			row.xMin,
			row.yMax,
			row.xMax,
			row.yMin,
			row.outputFile)
	}
}

func writeRowsToFile(rows []row, filename string) error {
	f, err := os.Open(filename)
	if err != nil {
		return nil
	}
	writeRows(rows, f)
	return f.Close()
}

func stringVar(long, short, value, usage string) *string {
	p := flag.String(long, value, usage)
	flag.StringVar(p, short, value, usage+" (shorthand)")
	return p
}

func boolVar(long, short string, value bool, usage string) *bool {
	p := flag.Bool(long, value, usage)
	flag.BoolVar(p, short, value, usage+" (shorthand)")
	return p
}

func isFile(filename string) bool {
	fi, err := os.Stat(filename)
	if err != nil {
		log.Printf("error: %v\n", err)
		return false
	}
	return fi.Mode().IsRegular()
}

func isDir(dirname string) bool {
	di, err := os.Stat(dirname)
	if err != nil {
		log.Printf("error: %v\n", err)
		return false
	}
	return di.IsDir()
}

func main() {

	var (
		pre         = flag.String("pre", "", "prefix of the new filename in output")
		post        = flag.String("post", "", "postfix of the new filename in output")
		gdalCmdName = stringVar("gdal_cmd_name", "c", "", "Set name of the gdalwarp cmd")
		gdalCmdFile = stringVar(
			"gdal_cmd_file", "f", gdalwarpCmdFile, "Set name of the gdalwarp cmd")
		dryRun     = boolVar("dry_run", "d", false, "test run (just print the commands)")
		debug1File = flag.String(
			"debug1", "", "Write out grid config semicolon seperated")
		debug2File = flag.String(
			"debug2", "", "Write out gdalwarp commands for debugging")
	)

	flag.Parse()

	if n := flag.NArg(); n != 4 {
		log.Printf(
			"error: Wrong number of arguments - should be 4 found %d\n", n)
		os.Exit(1)
	}
	var (
		shpGridNew = flag.Arg(0)
		shpGrid    = flag.Arg(1)
		sourceDir  = flag.Arg(2)
		destDir    = flag.Arg(3)
	)

	if !isFile(shpGridNew) {
		log.Printf("error: Cannot open shapefile %q\n", shpGridNew)
		os.Exit(2)
	}
	if !isFile(shpGrid) {
		log.Printf("error: Cannot open shapefile %q\n", shpGrid)
		os.Exit(3)
	}
	if !isDir(sourceDir) {
		log.Printf("error: Cannot open sourcedir %q\n", sourceDir)
		os.Exit(3)
	}
	if !isDir(destDir) {
		log.Printf("error: Cannot open destdir %q\n", destDir)
		os.Exit(3)
	}
	if !isFile(*gdalCmdFile) {
		log.Printf("error: Cannot open gdal command file %q\n", *gdalCmdFile)
		os.Exit(5)
	}
	if *gdalCmdName == "" {
		log.Println("error: No gdalwrap command name is given")
		os.Exit(4)
	}
	cmd, err := getGdalwarpCommand(*gdalCmdFile, *gdalCmdName)
	if err != nil {
		log.Printf("error: %v", err)
		os.Exit(4)
	}

	rows, err := processShapes(shpGridNew, shpGrid, *pre, *post)
	if err != nil {
		log.Printf("error: %v\n", err)
		os.Exit(4)
	}

	if *debug1File != "" {
		log.Printf("info: Write debug configuration to %q\n", *debug1File)
		if err := writeRowsToFile(rows, *debug1File); err != nil {
			log.Printf("error: %v\n", err)
			os.Exit(4)
		}
	}
	if *dryRun {
		writeRows(rows, os.Stdout)
	}

	log.Println("info: Processing gdalwarp")

	if err := process(
		rows,
		sourceDir, destDir,
		cmd,
		*debug2File, *dryRun,
	); err != nil {
		log.Printf("error: %v\n", err)
		os.Exit(4)
	}
}

#!/usr/bin/env python3
""" Correct and complete the sbom for pix4speed towards BSI TR-3138-2.pdf v1.1 .

Reads and writes back a file from
    https://github.com/CycloneDX/cyclonedx-gomod
        developed for v1.8.0
    for https://heptapod.host/intevation/pix4speed/

SPDX-FileCopyrightText: 2024 Intevation GmbH <https://intevation.de>
SPDX-License-Identifier: GPL-2.0-or-later
"""

import json
import hashlib
import sys

def complete_sbom(sbom):
    # remove MD5 sum, as md5 is broken and thus does not make sense, also see
    # https://github.com/CycloneDX/cyclonedx-gomod/issues/529
    if sbom['metadata']['tools'][0]['hashes'][0]['alg'] == "MD5":
        del sbom['metadata']['tools'][0]['hashes'][0]


    # we know our license so we remove the generated value and add our own
    if 'evidence' in sbom['metadata']['component']:
        del sbom['metadata']['component']['evidence']
    sbom['metadata']['licenses'] = [{'license': {'id': 'GPL-2.0-or-later'}}]

    # add Intevation as entry that created the SBOM automatically
    sbom['metadata']['manufacturer'] = json.loads("""
        {
          "name": "Intevation GmbH",
          "url": ["https://intevation.de"]
        }
        """)

    # https://cyclonedx.org/docs/1.6/json/#metadata_component_name says
    #  The name of the component.
    #  This will often be a shortened, single name of the component.
    sbom['metadata']['component']['name'] = "pix4speed"

    sbom['metadata']['component']['publisher'] = "Intevation GmbH"
    sbom['metadata']['component']['authors'] = [{
        "name":"Bernhard Reiter",
        "email": "bernhard.reiter@intevation.de",
        }]

    add_hash_for_binary(sbom)

    sbom['metadata']['component']['externalReferences'] = [{
        "url": "https://heptapod.host/intevation/pix4speed/",
        "type": "vcs",
        }]

    # There is no single hash value of the source and no single URI of
    # the executable form. So we do not add it.

    # add license for pkg:golang/std
    # evidence found at https://go.dev/LICENSE
    for i, c in enumerate(sbom['components']):
        if c['name'] == 'std' and 'evidence' not in c['name']:
            sbom['components'][i]['evidence'] = {
                    "licenses":[{'license': {'id': 'BSD-3-Clause'}}]}


def add_hash_for_binary(sbom):
    sha256 = hashlib.sha256()

    with open("./bin/dataloader", "rb") as b:
        sha256.update(b.read())

    sbom['metadata']['component']['hashes'] = [{
        "alg": "SHA-256",
        "content": sha256.hexdigest(),
        }]


def run(filename):
    with open(filename) as f:
        sbom = json.load(f)

    complete_sbom(sbom)

    with open(filename, "wt") as w:
        json.dump(sbom, w, indent=2)


if __name__ == '__main__':
    run(sys.argv[1])

<!--
 SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
 SPDX-License-Identifier: GPL-2.0-or-later
-->

# Configuration

The configuration is a [TOML](https://toml.io/en/) file.

See [demo.properties](demo.properties) as an example.

## Difference to old properties files

The main differences between the old properties files and
the new TOML files are:

* `True` and `False` are now spelled `true` and `false`.
* Strings are now quoted in `"`.
* List of integers are now in `[`/`]` brackets. See `gdal_gdaladdoLevels`.

The logging in section `[logging]` was simplified to a
`level` and a `file` to append the output to.

## New features

In the `[DEFAULT]` section is a new int value value `scale`
which defaults to `1000` and represents the internal
representation in meters. This influences the values of
`minX`, `minY`, `maxX`, `maxY` and `extent`. There configured values
are multiplied by the value of `scale` to get there values
in meters. The internal resolution of the system are meters.
In generated filename which contain coordinates the components
are divided by `scale` before being interpolated in the final name.

The usage of the PostgreSQL is now optional.
Simply comment out or remove the `db_user`, `db_creds`, `db_name`, `db_host`. `db_port`
lines in the `[metaparser]` section of properties file:

```toml
#db_user="pix4speed"
#db_creds="pix4speed"
#db_name="pix4speed"
#db_host="localhost"
#db_port=5432
```

### Logging

Logging is configured in the properties file:

```toml
level = "DEBUG"
file = "/where/you/like/your/dataloader.log"
```

Additionally you can pass `-l level`/`--loglevel=level` `-f file`/`--logfile=file`
to the command line flags of the dataloader to overwrite these setting.

`-f -` logs to STDOUT. Adding `+` before the filename adds a second logging target.
This can be a file or STDOUT. `--logfile=+-` logs to the file configured in the
properties file AND to STDOUT.


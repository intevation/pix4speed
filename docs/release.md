<!--
 SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
 SPDX-License-Identifier: GPL-2.0-or-later
-->

# Create a release

To create a new release just add a new tag:
https://heptapod.host/intevation/pix4speed/-/tags/new.
Then the pipeline will automatically do a release and take the tag name
to use it for the release name and the compressed binaries.

Optionally edit the release in the heptapod web interface to add release notes.

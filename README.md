<!--
 SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
 SPDX-License-Identifier: GPL-2.0-or-later
-->

# pix4speed

`pix4speed` is a tool collection to build and maintain tile pyramids
to be served by [MapServer](https://mapserver.org) as
[WMS](https://www.ogc.org/standard/wms/).

⚠ **This is work in progress**. The current focus is on replacing
the old `dataloader`.

## Build

The `dataloader` is written in [Go](https://go.dev/). To build the Software
you need at least version 1.20 or later. You can get it [here](https://go.dev/dl/).

Dowloading the sources with

```shell
wget https://heptapod.host/intevation/pix4speed/-/archive/main/pix4speed-main.tar.gz
tar xfvz pix4speed-main.tar.gz
mv pix4speed-main pix4speed
cd pix4speed
```

or check them out with

```shell
git clone https://heptapod.host/intevation/pix4speed.git
cd pix4speed
```

Building the `dataloader`:

```shell
CGO_ENABLED=0 go build -o bin/dataloader ./cmd/dataloader
```

(We are using `CGO_ENABLED=0` to get a static binary which works on
a wide array of GNU/Linux systems.)

If you want to cross-compile on a Microsoft Windows Plattform
to target a GNU/Linux platform you can have to set the environment variables
`GOARCH` and `GOOS` before the build, eg.

```shell
SET GOOS=linux
SET GOARCH=amd64
```

The main pyramid tool is `bin/dataloader`. Place this into your `PATH`.

## External tools

To run the `dataloader` you need some extra tools installed:

* `xmllint` from [Libxml2](https://gitlab.gnome.org/GNOME/libxml2/-/wikis/home) to validate XML files.
* The [GDAL](https://gdal.org/programs/index.html) raster programs
  * `gdalinfo`
  * `gdalbuildvrt`
  * `gdal_translate`
  * `gdaladdo`
* `shptree` from [MapServer](https://mapserver.org/utilities/shptree.html)
  to create spatial indices for shapefiles.

To install it on OpenSUSE Leap 15.4 use:

```shell
sudo zypper addrepo https://download.opensuse.org/repositories/Application:Geo/15.4/Application:Geo.repo
sudo zypper addrepo https://download.opensuse.org/repositories/M17N:l10n.opensuse.org:Backports-2022.11/15.4/M17N:l10n.opensuse.org:Backports-2022.11.repo
sudo zypper refresh
sudo zypper install gdal
sudo zypper install mapserver
sudo zypper install libxml2-tools
```

For other OpenSUSE like distributions use [software.opensuse.org](https://software.opensuse.org/)
to find installation instructions for suited packages.

## Configuration

See [Configuration](./docs/configuration.md) for details.

## Usage

```shell
bin/dataloader --help
Usage:
dataloader cron                                - for starting the cron to update dops
dataloader loadmetadatafile <MetadataFileName> - to insert or update a single metadata-file into database
dataloader initpyramid                         - to initialize a new Image-Pyramid

Option:
-p propertiesFile | --properties=propertiesFile - to use a different Properties file
-l level | --loglevel=level                     - set log level
-f file  | --logfile=file                       - set log file ("-" for STDOUT,
                                                                prefix with "+" to add a second output).
-V | --version                                  - show version information
```

If no `propertiesFile` is given `default.properties` is used.

## License

&copy; 2023 by Intevation GmbH.
This is Free Software
covered by the terms of the [GPL-2.0-or-later](./LICENSES/GPL-2.0-or-later.txt).

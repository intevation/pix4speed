// SPDX-FileCopyrightText: 2023 Intevation GmbH <https://intevation.de>
//
// SPDX-License-Identifier: GPL-2.0-or-later

module heptapod.host/intevation/pix4speed

go 1.21

toolchain go1.22.7

require (
	github.com/BurntSushi/toml v1.4.0
	github.com/google/shlex v0.0.0-20191202100458-e7afc7fbc510
	github.com/jackc/pgx/v5 v5.7.1
	github.com/jonas-p/go-shp v0.1.1
	golang.org/x/net v0.29.0
)

require (
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgservicefile v0.0.0-20240606120523-5a60cdf6a761 // indirect
	golang.org/x/crypto v0.27.0 // indirect
	golang.org/x/text v0.18.0 // indirect
)
